package org.ustimov.messenger.services;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import org.ustimov.messengerclient.MessengerClient;
import org.ustimov.messengerclient.models.RequestContact;
import org.ustimov.messengerclient.requests.AddContactRequest;
import org.ustimov.messengerclient.requests.AuthRequest;
import org.ustimov.messengerclient.requests.ContactListRequest;
import org.ustimov.messengerclient.requests.DeleteContactRequest;
import org.ustimov.messengerclient.requests.ImportContactsRequest;
import org.ustimov.messengerclient.requests.SetUserInfoRequest;
import org.ustimov.messengerclient.requests.deprecated.ChannelListRequest;
import org.ustimov.messengerclient.requests.deprecated.CreateChannelRequest;
import org.ustimov.messengerclient.requests.deprecated.EnterChannelRequest;
import org.ustimov.messengerclient.requests.IRequest;
import org.ustimov.messengerclient.requests.deprecated.LeaveChannelRequest;
import org.ustimov.messengerclient.requests.RegisterRequest;
import org.ustimov.messengerclient.requests.SendMessageRequest;
import org.ustimov.messengerclient.requests.UserInfoRequest;
import org.ustimov.messengerclient.services.Attach;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

/**
 * Created by ustimov on 08/11/15.
 */
public class MessagingService extends Service {

    IBinder mBinder = new LocalBinder();

    final String mHost = "188.166.49.215";
    final int mPort = 7788;

    MessengerClient mMessengerClient;

    public void onCreate() {
        super.onCreate();
        mMessengerClient = new MessengerClient(mHost, mPort);
        Log.d("Service", "onCreate");
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("Service", "onStartCommand");
        //connect();
        return super.onStartCommand(intent, flags, startId);
    }



    public void onDestroy() {
        super.onDestroy();
        Log.d("Service", "onDestroy");
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public void connect() {
        /*
        try {
            mMessengerClient.connect();
        } catch (IOException e) {
            Log.d("[SERVICE]", "ERROR WHILE CLIENT OPEN");
        }
        */
        new Thread(new OpenHelper()).start();
    }

    public void addEventListener(MessengerClient.EventListener listener) {
        mMessengerClient.addEventListener(listener);
    }

    public void removeEventListener(MessengerClient.EventListener listener) {
        mMessengerClient.removeEventListener(listener);
    }

    public class LocalBinder extends Binder {
        public MessagingService getServiceInstance() {
            return MessagingService.this;
        }
    }

    public void send(IRequest request, MessengerClient.ResponseListener listener) {
        mMessengerClient.send(request, listener);
    }

    class OpenHelper implements Runnable {

        @Override
        public void run() {

            try {
                mMessengerClient.connect();
                /*
                InetAddress inetAddress = InetAddress.getByName("188.166.49.215");
                mSocket = new Socket(inetAddress, 7777);
                InputStream inputStream = mSocket.getInputStream();
                while (inputStream.available() == 0) {
                    Thread.sleep(100);

                }
                BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
                //Log.d("Service", in);
                */
            } catch (Exception e) {
                Log.d("Service", e.getMessage());
            }

        }
    }
    /*


    public IResponse Send(IRequest request) {
        Frame frame = new Frame();
        frame.action = request.getAction();
        //frame.data
    }
    */

    /* region Requests */
    public void sendChannelListRequest(MessengerClient.ResponseListener listener) {
        ChannelListRequest channelListRequest = new ChannelListRequest();
        channelListRequest.setAuthData(SessionManager.getUserId(this),
                SessionManager.getSessionId(this));
        mMessengerClient.send(channelListRequest, listener);
    }

    public void sendContactListRequest(MessengerClient.ResponseListener listener) {
        ContactListRequest contactListRequest = new ContactListRequest();
        contactListRequest.setAuthData(SessionManager.getUserId(this),
                SessionManager.getSessionId(this));
        mMessengerClient.send(contactListRequest, listener);
    }

    public void sendEnterChannelRequest(MessengerClient.ResponseListener listener,
                                        String channelId) {
        EnterChannelRequest enterChannelRequest = new EnterChannelRequest(channelId);
        enterChannelRequest.setAuthData(SessionManager.getUserId(this),
                SessionManager.getSessionId(this));
        mMessengerClient.send(enterChannelRequest, listener);
    }

    public void sendLeaveChannelRequest(MessengerClient.ResponseListener listener,
                                        String channelId) {
        LeaveChannelRequest leaveChannelRequest = new LeaveChannelRequest(channelId);
        leaveChannelRequest.setAuthData(SessionManager.getUserId(this),
                SessionManager.getSessionId(this));
        mMessengerClient.send(leaveChannelRequest, listener);
    }

    public void sendMessageToChannelRequest(MessengerClient.ResponseListener listener,
                                            String channelId, String text) {
        /*
        SendMessageRequest sendMessageRequest = new SendMessageRequest(
                channelId, text);
        sendMessageRequest.setAuthData(SessionManager.getUserId(this),
                SessionManager.getSessionId(this));
        mMessengerClient.send(sendMessageRequest, listener);
        */
        Log.d("MESSAGING SERVICE", "NOT IMPLEMENTED SEND MESSAGE");
    }

    public void sendRegisterRequest(MessengerClient.ResponseListener listener, String email,
                                String password, String nickname) {
        RegisterRequest registerRequest = new RegisterRequest(email, md5(password), nickname);
        mMessengerClient.send(registerRequest, listener);
    }

    public void sendAuthRequest(MessengerClient.ResponseListener listener, String email,
                                String password) {
        AuthRequest authRequest = new AuthRequest(email, md5(password));
        mMessengerClient.send(authRequest, listener);
    }

    public void sendUserInfoRequest(MessengerClient.ResponseListener listener, String userId) {
        UserInfoRequest userInfoRequest = new UserInfoRequest(userId);
        userInfoRequest.setAuthData(SessionManager.getUserId(this),
                SessionManager.getSessionId(this));
        mMessengerClient.send(userInfoRequest, listener);
    }

    public void sendSetUseInfoRequest(MessengerClient.ResponseListener listener, String userStatus,
                                      String email, String phone, String picture) {
        SetUserInfoRequest setUserInfoRequest = new SetUserInfoRequest(userStatus, email, phone,
                picture);
        setUserInfoRequest.setAuthData(SessionManager.getUserId(this),
                SessionManager.getSessionId(this));
        mMessengerClient.send(setUserInfoRequest, listener);
    }

    public void sendCreateChannelRequest(MessengerClient.ResponseListener listener,
                                         String channelName, String channelDescription) {
        CreateChannelRequest createChannelRequest = new CreateChannelRequest(channelName,
                channelDescription);
        createChannelRequest.setAuthData(SessionManager.getUserId(this),
                SessionManager.getSessionId(this));
        mMessengerClient.send(createChannelRequest, listener);
    }

    public void sendAddContactRequest(MessengerClient.ResponseListener listener, String userId) {
        AddContactRequest addContactRequest = new AddContactRequest(userId);
        addContactRequest.setAuthData(SessionManager.getUserId(this),
                SessionManager.getSessionId(this));
        mMessengerClient.send(addContactRequest, listener);
    }

    public void sendMessageRequest(MessengerClient.ResponseListener listener, String userId,
                                   String message, Attach attach) {
        SendMessageRequest sendMessageRequest = new SendMessageRequest(userId, message, attach);
        sendMessageRequest.setAuthData(SessionManager.getUserId(this),
                SessionManager.getSessionId(this));
        mMessengerClient.send(sendMessageRequest, listener);
    }

    public void sendDeleteContactRequest(MessengerClient.ResponseListener listener, String userId) {
        DeleteContactRequest deleteContactRequest = new DeleteContactRequest(userId);
        deleteContactRequest.setAuthData(SessionManager.getUserId(this),
                SessionManager.getSessionId(this));
        mMessengerClient.send(deleteContactRequest, listener);
    }

    public void sendImportContactsRequest(MessengerClient.ResponseListener listener,
                                          ArrayList<RequestContact> contacts) {
        ImportContactsRequest importContactsRequest = new ImportContactsRequest(contacts);
        importContactsRequest.setAuthData(SessionManager.getUserId(this),
                SessionManager.getSessionId(this));
        mMessengerClient.send(importContactsRequest, listener);

    }
    /* end region */

    public static String md5(String s) {
        String md5sum = null;
        try {
            MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++)
                hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
            md5sum = hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return md5sum;
    }
}
