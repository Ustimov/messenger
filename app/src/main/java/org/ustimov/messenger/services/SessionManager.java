package org.ustimov.messenger.services;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by ustimov on 19/11/15.
 */
public class SessionManager {

    private static String PREF_TAG = "AuthData";
    private static String USER_ID_TAG = "UserId";
    private static String SESSION_ID_TAG = "SessionId";
    private static String EMAIL_TAG = "Email";
    private static String PASSWORD_TAG = "Password";

    private static String mUserId;
    private static String mSessionId;
    private static String mEmail;
    private static String mPassword;

    public static boolean onlySettings = false;

    public static String getUserId(Context context) {
        if (mUserId == null) {
            mUserId = getValueFromSharedPreferences(context, USER_ID_TAG);
        }
        return mUserId;
    }

    public static String getSessionId(Context context) {
        if (mSessionId == null) {
            mSessionId = getValueFromSharedPreferences(context, SESSION_ID_TAG);
        }
        return  mSessionId;
    }

    public static String getEmail(Context context) {
        if (mEmail == null) {
            mEmail = getValueFromSharedPreferences(context, EMAIL_TAG);
        }
        return mEmail;
    }

    public static String getPassword(Context context) {
        if (mPassword == null) {
            mPassword = getValueFromSharedPreferences(context, PASSWORD_TAG);
        }
        return mPassword;
    }

    public static void setAuthData(Context context, String email, String password) {
        mEmail = email;
        mPassword = password;
        saveAuthDataToSharedPreferences(context);
    }

    public static void setCredenals(Context context, String userId, String sessionId) {
        mUserId = userId;
        mSessionId = sessionId;
        saveCredenalsToSharedPreferences(context);
    }

    private static String getValueFromSharedPreferences(Context context, String tag) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                PREF_TAG, Context.MODE_PRIVATE);
        return sharedPreferences.getString(tag, null);
    }

    private static void saveCredenalsToSharedPreferences(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                PREF_TAG, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(USER_ID_TAG, mUserId);
        editor.putString(SESSION_ID_TAG, mSessionId);
        editor.commit();
    }

    private static void saveAuthDataToSharedPreferences(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                PREF_TAG, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(EMAIL_TAG, mEmail);
        editor.putString(PASSWORD_TAG, mPassword);
        editor.commit();
    }
}
