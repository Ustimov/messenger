package org.ustimov.messenger.services;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;

/**
 * Created by ustimov on 19/11/15.
 */
public class MessagingServiceConnection implements ServiceConnection {

    private boolean mBounded = false;
    private MessagingService mMessagingService = null;
    private OnConnectListener mListener;

    public MessagingServiceConnection(OnConnectListener listener) {
        mListener = listener;
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        //mBounded = true;
        MessagingService.LocalBinder mLocalBinder = (MessagingService.LocalBinder)service;
        mMessagingService = mLocalBinder.getServiceInstance();
        mListener.onConnect();
        Log.d("ServiceConnection", "Service is bounded");
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        //mBounded = false;
        mMessagingService = null;
        Log.d("ServiceConnection", "Service is unbounded");
    }

    public void setBounded(boolean isBounded) {
        this.mBounded = isBounded;
    }

    public MessagingService getMessagingService() {
        return mMessagingService;
    }

    public boolean isBounded() {
        return mBounded;
    }

    public interface OnConnectListener {
        public void onConnect();
    }
}
