package org.ustimov.messenger.adapters;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.ustimov.messenger.R;
import org.ustimov.messengerclient.models.ContactModel;
import org.ustimov.messengerclient.models.ResponseContact;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by ustimov on 05/01/16.
 */
public class ContactListAdapter extends BaseAdapter {
    private Activity mActivity;
    private ArrayList<ContactModel> mContacts;

    public ContactListAdapter(Activity activity, ArrayList<ContactModel> contacts) {
        mActivity = activity;
        mContacts = contacts;//new ArrayList<>(Arrays.asList(contacts));
    }

    @Override
    public int getCount() {
        return mContacts.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (view == null) {
            view = mActivity.getLayoutInflater().inflate(R.layout.contactlist_item, null);
        }

        ContactModel contact = mContacts.get(position);

        TextView nicknameTextView = (TextView) view.findViewById(R.id.nicknameTextView);
        nicknameTextView.setText(contact.getNickname());

        TextView lastMessageTextView = (TextView) view.findViewById(R.id.lastMessageTextView);
        lastMessageTextView.setText(contact.getLastMessage());

        //TextView unreadMsgCountTextView = (TextView) view.findViewById(R.id.unreadMsgCountTextView);

        ImageView userPictureImageView = (ImageView) view.findViewById(R.id.userPictureImageView);
        String picture = contact.getPicture();
        if (picture != null && !picture.equals("")) {
            byte[] decodedString = Base64.decode(picture, Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            userPictureImageView.setImageBitmap(decodedByte);
        }

        return view;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Object getItem(int position) {
        return mContacts.get(position);
    }
}
