package org.ustimov.messenger.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.ustimov.messenger.R;
import org.ustimov.messengerclient.models.Channel;

import java.util.ArrayList;

/**
 * Created by ustimov on 17/11/15.
 */
public class ChannelListAdapter extends BaseAdapter {

    private Activity context;
    private ArrayList<Channel> mChannels = new ArrayList<>();

    public ChannelListAdapter(Activity context, Channel[] channels) {
        super();
        this.context = context;
        //mChannels = channels;
        //if (channels != null) {
            for (Channel channel : channels) {
                mChannels.add(channel);
            }
        //}
    }

    @Override
    public int getCount() {
        return mChannels.size();
    }

    @Override
    public Object getItem(int position) {
        return mChannels.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (view == null) {
            view = context.getLayoutInflater().inflate(R.layout.chatroom_item, null);
        }

        TextView channelNameTextView = (TextView) view.findViewById(R.id.channelNameTextView);
        channelNameTextView.setText(mChannels.get(position).getName());

        TextView channelDescriptionTextView = (TextView) view.findViewById(
                R.id.channelDescriptionTextView);
        channelDescriptionTextView.setText(mChannels.get(position).getDescription());

        TextView channelCountListView = (TextView)view.findViewById(R.id.channelCountTextView);
        channelCountListView.setText(mChannels.get(position).getFormatedOnline());

        return view;
    }
}
