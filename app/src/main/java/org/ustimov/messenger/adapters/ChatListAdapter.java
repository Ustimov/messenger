package org.ustimov.messenger.adapters;

import android.app.Activity;
import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import org.ustimov.messenger.MainActivity;
import org.ustimov.messenger.R;
import org.ustimov.messengerclient.models.MessageModel;
import org.ustimov.messengerclient.models.UserType;
import org.ustimov.messengerclient.models.Message;
import org.ustimov.messengerclient.services.Attach;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class ChatListAdapter extends BaseAdapter implements View.OnClickListener {

    private ArrayList<MessageModel> mMessages;
    private Context mActivity;
    public static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("HH:mm");

    public ChatListAdapter(Activity activity, ArrayList<MessageModel> messages) {
        mMessages = messages;
        mActivity = activity;
        if (!(activity instanceof OnSaveAttachmentListener)) {
            throw new ClassCastException(activity.toString() +
                    " must implement OnSaveAttachmentListener");
        }
    }

    public interface OnSaveAttachmentListener {
        public void onSaveAttachment(String type, String data);
    }

    @Override
    public int getCount() {
        return mMessages.size();
    }

    public void addMessage(MessageModel message) {
        mMessages.add(message);
    }

    @Override
    public Object getItem(int position) {
        return mMessages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = null;
        MessageModel message = mMessages.get(position);
        SelfViewHolder selfHolder;
        OtherViewHolder otherHolder;

        if (message.getUserType() == UserType.SELF) {
            if (convertView == null) {
                v = LayoutInflater.from(mActivity).inflate(R.layout.chat_user2_item, null, false);
                selfHolder = new SelfViewHolder();
                selfHolder.messageTextView = (TextView) v.findViewById(R.id.chatSelfMessageTextView);
                selfHolder.timeTextView = (TextView) v.findViewById(R.id.chatSelfTimeTextView);
                selfHolder.attachTextView = (TextView) v.findViewById(R.id.chatAttachInfoTextView);
                selfHolder.getAttachImageButton = (ImageButton) v.findViewById(R.id.chatGetAttachButton);
                v.setTag(selfHolder);
            } else {
                v = convertView;
                selfHolder = (SelfViewHolder) v.getTag();
            }
            try {
                selfHolder.messageTextView.setText(message.getText());
                long time = Long.parseLong(message.getTime());
                selfHolder.timeTextView.setText(SIMPLE_DATE_FORMAT.format(time));
                Attach attach = message.getAttach();
                if (attach.getType() != null && attach.getData() != null) {
                    selfHolder.attachTextView.setText("Attached: " + attach.getType());
                    selfHolder.getAttachImageButton.setTag(R.id.attach_type, attach.getType());
                    selfHolder.getAttachImageButton.setTag(R.id.attach_data, attach.getData());
                    selfHolder.getAttachImageButton.setOnClickListener(this);
                    selfHolder.attachTextView.setVisibility(View.VISIBLE);
                    selfHolder.getAttachImageButton.setVisibility(View.VISIBLE);
                } else {
                    selfHolder.attachTextView.setVisibility(View.GONE);
                    selfHolder.getAttachImageButton.setVisibility(View.GONE);
                }
            } catch (Exception e) {
                Exception ex = e;
            }
        } else if (message.getUserType() == UserType.OTHER) {
            if (convertView == null) {
                v = LayoutInflater.from(mActivity).inflate(R.layout.chat_user1_item, null, false);
                otherHolder = new OtherViewHolder();
                otherHolder.messageTextView = (TextView) v.findViewById(R.id.chatOtherMessageTextView);
                otherHolder.timeTextView = (TextView) v.findViewById(R.id.chatOtherMessageTime);
                otherHolder.userNameTextView = (TextView) v.findViewById(R.id.chatOtherMessageNick);
                otherHolder.attachTextView = (TextView) v.findViewById(R.id.chatAttachInfoTextView);
                otherHolder.getAttachImageButton = (ImageButton) v.findViewById(R.id.chatGetAttachButton);
                v.setTag(otherHolder);
            } else {
                v = convertView;
                otherHolder = (OtherViewHolder) v.getTag();
            }
            otherHolder.messageTextView.setText(message.getText());
            long time = Long.parseLong(message.getTime() + "000");
            otherHolder.timeTextView.setText(SIMPLE_DATE_FORMAT.format(time));
            otherHolder.userNameTextView.setText(message.getNickname());
            Attach attach = message.getAttach();
            String type = attach.getType();
            String data = attach.getData();
            if (type != null && !type.isEmpty() && data != null && !data.isEmpty()) {
                otherHolder.attachTextView.setText("Attached: " + attach.getType());
                otherHolder.getAttachImageButton.setTag(R.id.attach_type, attach.getType());
                otherHolder.getAttachImageButton.setTag(R.id.attach_data, attach.getData());
                otherHolder.getAttachImageButton.setOnClickListener(this);
                otherHolder.attachTextView.setVisibility(View.VISIBLE);
                otherHolder.getAttachImageButton.setVisibility(View.VISIBLE);
            } else {
                otherHolder.attachTextView.setVisibility(View.GONE);
                otherHolder.getAttachImageButton.setVisibility(View.GONE);
            }
        }

        return v;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        MessageModel message = mMessages.get(position);
        return message.getUserType().ordinal();
    }

    @Override
    public void onClick(View v) {
        String type = (String) v.getTag(R.id.attach_type);
        String data = (String) v.getTag(R.id.attach_data);
        Log.d(type, data);

        ((OnSaveAttachmentListener)mActivity).onSaveAttachment(type, data);
    }

    private class SelfViewHolder {
        public TextView messageTextView;
        public TextView timeTextView;
        public TextView attachTextView;
        public ImageButton getAttachImageButton;
    }

    private class OtherViewHolder extends SelfViewHolder {
        public TextView userNameTextView;
    }
}

