package org.ustimov.messenger;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;

import org.ustimov.messenger.adapters.ContactListAdapter;
import org.ustimov.messenger.fragments.ContactListFragment;
import org.ustimov.messenger.fragments.RegisterFragment;

import java.util.Stack;

/**
 * Created by ustimov on 21/11/15.
 */
public class AdvancedFragmentManager {

    private static Stack<Fragment> mFragmentStack = new Stack<>();

    private static ContactListFragment mContactListFragment = new ContactListFragment();

    public static void changeFragment(Activity activity, int containerId, Fragment fragment,
                                      boolean saveInBackStack, String tag) {
        FragmentManager fragmentManager = activity.getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(containerId, fragment, tag);
        if (saveInBackStack) {
            fragmentTransaction.addToBackStack(null);
        }
        fragmentTransaction.commit();
    }

    public static void changeFragment(Activity activity, int containerId, Fragment fragment) {
        FragmentManager fragmentManager = activity.getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(containerId, fragment, null);
        fragmentTransaction.commit();
        if (fragment != mContactListFragment) {
            mFragmentStack.push(fragment);
        }
    }

    public static Fragment getFragment() {
        if (mFragmentStack.empty()) {
            return  mContactListFragment;
        } else {
            return mFragmentStack.pop();
        }
    }

    public static ContactListFragment getContactListFragment() {
        return mContactListFragment;
    }

    public static int getCount() {
        return mFragmentStack.size();
    }
}
