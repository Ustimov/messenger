package org.ustimov.messenger;

import android.Manifest;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.ustimov.messenger.adapters.ChatListAdapter;
import org.ustimov.messenger.adapters.ContactListAdapter;
import org.ustimov.messenger.fragments.AddContactFragment;
import org.ustimov.messenger.fragments.ChatFragment;
import org.ustimov.messenger.fragments.ContactListFragment;
import org.ustimov.messenger.fragments.DeleteContactDialogFragment;
import org.ustimov.messenger.fragments.FileNameDialogFragment;
import org.ustimov.messenger.fragments.deprecated.CreateChannelFragment;
import org.ustimov.messenger.fragments.ProfileFragment;
import org.ustimov.messenger.fragments.SettingsFragment;

import org.ustimov.messenger.fragments.UserInfoFragment;
import org.ustimov.messenger.services.MessagingService;
import org.ustimov.messenger.services.MessagingServiceConnection;
import org.ustimov.messenger.services.SessionManager;
import org.ustimov.messengerclient.MessengerClient;
import org.ustimov.messengerclient.events.IEvent;
import org.ustimov.messengerclient.events.SentMessageEvent;
import org.ustimov.messengerclient.events.TimeoutEvent;
import org.ustimov.messengerclient.models.ContactModel;
import org.ustimov.messengerclient.models.MessageModel;
import org.ustimov.messengerclient.models.RequestContact;
import org.ustimov.messengerclient.models.ResponseContact;
import org.ustimov.messengerclient.models.Storage;
import org.ustimov.messengerclient.models.UserType;
import org.ustimov.messengerclient.responses.AddContactResponse;
import org.ustimov.messengerclient.responses.ContactListResponse;
import org.ustimov.messengerclient.responses.DeleteContactResponse;
import org.ustimov.messengerclient.responses.IResponse;
import org.ustimov.messengerclient.responses.ImportContactsResponse;
import org.ustimov.messengerclient.responses.SetUserInfoResponse;
import org.ustimov.messengerclient.responses.UserInfoResponse;
import org.ustimov.messengerclient.services.Attach;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;

public class MainActivity extends BaseActivity implements
        AdapterView.OnItemClickListener,
        MessengerClient.EventListener, MessengerClient.ResponseListener,
        ChatFragment.OnFragmentInteractionListener, MessagingServiceConnection.OnConnectListener,
        CreateChannelFragment.OnFragmentInteractionListener,
        ProfileFragment.OnFragmentInteractionListener,
        AddContactFragment.OnFragmentInteractionListener,
        ContactListFragment.OnFragmentInteractionListener,
        DeleteContactDialogFragment.OnFragmentInteractionListener,
        ChatListAdapter.OnSaveAttachmentListener,
        FileNameDialogFragment.OnFragmentInteractionListener {

    private static boolean mIsFirstRun = true;

    private String[] mNavigationDrawerOptions;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    private boolean mIsProfileRequested = false;

    private ProfileFragment mProfileFragment;
    private ChatFragment mChatFragment;

    private void initActivity() {
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mNavigationDrawerOptions = new String[] { "Import Contacts", "Profile", "Settings", "Logout" };
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        mDrawerList.setAdapter(new ArrayAdapter<>(this,
                R.layout.drawer_item, R.id.drawer_item_text, mNavigationDrawerOptions));

        mDrawerList.setOnItemClickListener(this);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,toolbar,
                R.string.drawer_open, R.string.drawer_close) {

            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
            }
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initActivity();

        String userId = getIntent().getStringExtra("UserId");
        getIntent().removeExtra("UserId");

        if (userId != null) {
            ContactModel contact = Storage.getContact(userId);
            mChatFragment = new ChatFragment();
            mChatFragment.setAdapter(new ChatListAdapter(this, contact.getMessages()));
            mChatFragment.setContactId(userId);
            String nickname = getIntent().getStringExtra("Nickname");
            mChatFragment.setContactNickname(nickname);
            AdvancedFragmentManager.changeFragment(this, R.id.fragment_container, mChatFragment);
            return;
        }

        Fragment fragment = AdvancedFragmentManager.getFragment();
        if (fragment instanceof ContactListFragment) {
            ((ContactListFragment) fragment).setAdapter(
                    new ContactListAdapter(this, Storage.getContacts()));
        }

        AdvancedFragmentManager.changeFragment(this, R.id.fragment_container, fragment);
    }

    @Override
    protected void onStart() {
        super.onStart();

        Intent intent = new Intent(this, MessagingService.class);
        mServiceConnection = new MessagingServiceConnection(this);
        bindService(intent, mServiceConnection, 0);
        mServiceConnection.setBounded(true);
    }

    private void importContacts() {
        HashMap<String, RequestContact> contacts = new HashMap<>();
        ContentResolver contentResolver = getContentResolver();
        Cursor cursor = contentResolver.query(ContactsContract.Data.CONTENT_URI, null,
                ContactsContract.Data.MIMETYPE + "=? OR " +
                        ContactsContract.Data.MIMETYPE + "=?",
                new String[]{ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE,
                        ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE},
                ContactsContract.Data.CONTACT_ID);
        while (cursor.moveToNext()) {
            String contactName = cursor.getString(cursor.getColumnIndex(
                    ContactsContract.Data.DISPLAY_NAME));
            String data1 = cursor.getString(cursor.getColumnIndex(
                    ContactsContract.Data.DATA1));
            RequestContact contact;
            if (contacts.containsKey(contactName)) {
                contact = contacts.get(contactName);
            } else {
                contact = new RequestContact();
                contact.setUserId(SessionManager.getUserId(this));
                contact.setName(contactName);
                contacts.put(contactName, contact);
            }
            if (data1.contains("@")) {
                contact.setEmail(data1);
            } else {
                contact.setPhone(data1);
            }
        }
        cursor.close();
        ArrayList<RequestContact> contactList = new ArrayList<>();
        for (String key : contacts.keySet()) {
            RequestContact contact = contacts.get(key);
            /*
            Toast.makeText(MainActivity.this, "UserId: " + contact.myid + ", Name: " +
                    contact.name + ", Phone: " + contact.phone + ", Email: " + contact.email,
                    Toast.LENGTH_SHORT).show();
            */
            contactList.add(contact);
        }
        mMessagingService.sendImportContactsRequest(this, contactList);
    }

    private final int READ_CONTACTS_PERMISSION_REQUEST_RESULT = 0;

    @Override
    public void onItemClick(AdapterView parent, View view, int position, long id) {
        switch (position) {
            case 0:
                if (ContextCompat.checkSelfPermission(MainActivity.this,
                        Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                            Manifest.permission.READ_CONTACTS)) {
                        Toast.makeText(MainActivity.this, "Contact import requires read contacts" +
                                " permission!", Toast.LENGTH_LONG).show();
                    } else {
                        ActivityCompat.requestPermissions(MainActivity.this,
                                new String[]{Manifest.permission.READ_CONTACTS},
                                READ_CONTACTS_PERMISSION_REQUEST_RESULT);
                    }
                } else {
                    importContacts();
                }
                break;
            case 1:
                mMessagingService.sendUserInfoRequest(this, SessionManager.getUserId(this));
                mIsProfileRequested = true;
                break;
            case 2:
                AdvancedFragmentManager.changeFragment(this, R.id.fragment_container,
                        new SettingsFragment());
                break;
            case 3:
                logout();
                break;
        }

        mDrawerList.setItemChecked(position, true);
        mDrawerLayout.closeDrawer(mDrawerList);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[],
                                           int[] grantResults) {
        switch (requestCode) {
            case READ_CONTACTS_PERMISSION_REQUEST_RESULT: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    importContacts();
                }
                break;
            }
            case WRITE_EXTERNAL_PERMISSION_REQUEST_RESULT: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    showFileNameDialog(mAttachType, mAttachData);
                }
            }
        }
    }

    public void onAddContactClick() {
        AdvancedFragmentManager.changeFragment(this, R.id.fragment_container,
                new AddContactFragment());
    }

    @Override
    public void onBackPressed() {
        if (AdvancedFragmentManager.getCount() == 0) {
            super.onBackPressed();
        } else {
            AdvancedFragmentManager.getFragment(); // Pop current
            Fragment fragment = AdvancedFragmentManager.getFragment(); // Pop previous
            AdvancedFragmentManager.changeFragment(this, R.id.fragment_container, fragment);

            if (fragment instanceof ContactListFragment) {
                ((ContactListFragment) fragment).setAdapter(
                        new ContactListAdapter(this, Storage.getContacts()));
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if(mServiceConnection.isBounded()) {
            unbindService(mServiceConnection);
            mMessagingService.removeEventListener(this);
            mServiceConnection.setBounded(false);
        }
        //Intent intent = new Intent(this, MessagingService.class);
        //stopService(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onEvent(final IEvent event) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (event instanceof SentMessageEvent) {
                    if (((SentMessageEvent) event).from.equals(SessionManager
                            .getUserId(MainActivity.this))) {
                        return;
                    }
                    Log.d("MainActivity", "Message: " + ((SentMessageEvent) event).body);

                    SentMessageEvent sentMessageEvent = (SentMessageEvent) event;

                    MessageModel message = new MessageModel(sentMessageEvent.getUserId(),
                            sentMessageEvent.getNickname(), sentMessageEvent.getText(),
                            sentMessageEvent.getTime(), sentMessageEvent.getAttach(),
                            UserType.OTHER);

                    Storage.addMessage(message);

                    AdvancedFragmentManager.getContactListFragment().update();

                    showNotification(message);

                    if (mChatFragment != null) {
                        mChatFragment.update();
                    }

                } else if (event instanceof TimeoutEvent) {
                    Toast.makeText(MainActivity.this, "Internet connection error",
                            Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //MainActivity.this.finish();
                        }
                    }, 2000);
                }
                Log.d("MainActivity", "Event");
            }
        });
    }

    private void showNotification(MessageModel message) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(MainActivity.this)
                        .setSmallIcon(R.drawable.ic_chat_send)
                        .setContentTitle(message.getNickname())
                        .setContentText(message.getText());

        Intent resultIntent = new Intent(MainActivity.this, MainActivity.class);
        resultIntent.putExtra("UserId", message.getUserId());
        resultIntent.putExtra("Nickname", message.getNickname());

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(MainActivity.this);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        int id = 0;
        Notification notification = mBuilder.build();
        notification.flags = Notification.DEFAULT_LIGHTS | Notification.FLAG_AUTO_CANCEL;
        mNotificationManager.notify(id, notification);
    }

    @Override
    public void onResponse(final IResponse response) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MainActivity.this, response.getError(), Toast.LENGTH_SHORT).show();
                if (response instanceof UserInfoResponse && !mIsProfileRequested) {
                    processUserInfoResponse((UserInfoResponse) response);
                } else if (response instanceof SetUserInfoResponse) {
                    processSetUserInfoResponse((SetUserInfoResponse) response);
                } else if (response instanceof ImportContactsResponse) {
                    processImportContactResponse((ImportContactsResponse) response);
                } else if (response instanceof UserInfoResponse) {
                    processShowProfile((UserInfoResponse) response);
                } else if (response instanceof ContactListResponse) {
                    processContactListResponse((ContactListResponse) response);
                } else if (response instanceof AddContactResponse) {
                    processAddContactResponse((AddContactResponse) response);
                } else if (response instanceof DeleteContactResponse) {
                    processDeleteContactResponse((DeleteContactResponse) response);
                }
            }
        });
    }

    @Override
    public void onMessageSend(String message, String userId, Attach attach) {
        mMessagingService.sendMessageRequest(this, userId, message, attach);
    }

    @Override
    public void onUserClick(String userId) {
        mMessagingService.sendUserInfoRequest(this, userId);
    }

    @Override
    public void onConnect() {
        mMessagingService = mServiceConnection.getMessagingService();
        mMessagingService.addEventListener(this);
        mMessagingService.sendContactListRequest(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!SessionManager.onlySettings) {
            getMenuInflater().inflate(R.menu.menu_main, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.add_channel) {
            onAddContactClick();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void logout() {
        SessionManager.setAuthData(this, null, null);
        SessionManager.setCredenals(this, null, null);

        Storage.clear();

        Intent mainIntent = new Intent(MainActivity.this, SplashActivity.class);
        mainIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        this.startActivity(mainIntent);
        this.finish();
    }

    @Override
    public void onCreateButtonClick(String channelName, String channelDescription) {
        mMessagingService.sendCreateChannelRequest(this, channelName, channelDescription);
        Log.d("MainActivity", "Name: " + channelName + " Description: " + channelDescription);
    }

    @Override
    public void onSaveButtonClick(String userStatus, String email, String phone, String picture) {
        mMessagingService.sendSetUseInfoRequest(this, userStatus, email, phone, picture);
        onBackPressed();
    }

    /*
    public void setLoadingState(boolean saveToBackStack) {

        AdvancedFragmentManager.changeFragment(this, R.id.fragment_container,
                new LoadingFragment(), saveToBackStack, null);

        setDrawerState(false);
    }

    public void unsetLoadingState(boolean isSaved) {

        if (isSaved) {
            getFragmentManager().popBackStack();
        }

        setDrawerState(true);
    }
    */

    public void setDrawerState(boolean isEnabled) {
        if (isEnabled) {
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            mDrawerToggle.setDrawerIndicatorEnabled(true);
            mDrawerToggle.syncState();

        } else {
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            mDrawerToggle.setDrawerIndicatorEnabled(false);
            mDrawerToggle.syncState();
        }
    }

    private void processSetUserInfoResponse(SetUserInfoResponse response) {
        if (mProfileFragment != null) {
            mProfileFragment.updateInfo();
            Log.d("MainActivity", "mProfileFragment is null");
        }
    }

    private void processShowProfile(UserInfoResponse response) {
        mProfileFragment = new ProfileFragment();
        mProfileFragment.setUserInfo(response.getNickname(), response.getUserStatus(),
                response.getEmail(), response.getPhone(), response.getPicture());
        AdvancedFragmentManager.changeFragment(this, R.id.fragment_container, mProfileFragment);
        mIsProfileRequested = false;
    }

    private void processUserInfoResponse(UserInfoResponse response) {
        UserInfoFragment userInfoFragment = new UserInfoFragment();
        userInfoFragment.setUserInfo(response.getNickname(), response.getUserStatus(),
                response.getEmail(), response.getPhone(), response.getPicture());
        AdvancedFragmentManager.changeFragment(this, R.id.fragment_container, userInfoFragment);
    }

    private void processContactListResponse(ContactListResponse response) {
        Storage.setContacts(response.getContacts());
        AdvancedFragmentManager.getContactListFragment().update();
    }

    private void processAddContactResponse(AddContactResponse response) {
        onBackPressed();
        mMessagingService.sendContactListRequest(this);
    }

    private void processDeleteContactResponse(DeleteContactResponse response) {
        // Do nothing
    }

    private void processImportContactResponse(ImportContactsResponse response) {
        ResponseContact[] contacts = response.getContacts();
        Storage.setContacts(contacts);
        if (contacts.length == 0) {
            Toast.makeText(MainActivity.this, "No one of your contacts are registered!",
                    Toast.LENGTH_LONG).show();
        }
        AdvancedFragmentManager.getContactListFragment().update();
    }

    @Override
    public void onAddContactButtonClick(String userId) {
        Log.d("ADD_CONTACT", userId);
        mMessagingService.sendAddContactRequest(this, userId);
    }

    @Override
    public void onRemoveContact(final ContactModel contact) {
        DeleteContactDialogFragment deleteContactDialogFragment = new DeleteContactDialogFragment();
        deleteContactDialogFragment.setContact(contact);
        deleteContactDialogFragment.show(getFragmentManager(), "DeleteFragment");
    }

    @Override
    public void onContactClick(ContactModel contact) {
        if (mChatFragment == null) {
            mChatFragment = new ChatFragment();
        }
        AdvancedFragmentManager.changeFragment(this, R.id.fragment_container, mChatFragment);
        mChatFragment.setAdapter(new ChatListAdapter(this, contact.getMessages()));
        mChatFragment.setContactId(contact.getUserId());
        mChatFragment.setContactNickname(contact.getNickname());
    }

    @Override
    public void onConfirmContactDeleting(String userId) {
        Log.d("REMOVE", userId);
        mMessagingService.sendDeleteContactRequest(this, userId);
        Storage.deleteContact(userId);
        AdvancedFragmentManager.getContactListFragment().update();
    }

    private String mAttachType;
    private String mAttachData;

    final int WRITE_EXTERNAL_PERMISSION_REQUEST_RESULT = 1;

    @Override
    public void onSaveAttachment(String type, String data) {
        mAttachType = type;
        mAttachData = data;
        if (ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                Toast.makeText(MainActivity.this, "Saving of attached files requires write" +
                        " external storage permission!", Toast.LENGTH_LONG).show();
            } else {
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{ Manifest.permission.WRITE_EXTERNAL_STORAGE },
                        WRITE_EXTERNAL_PERMISSION_REQUEST_RESULT);
            }
        } else {
            showFileNameDialog(mAttachType, mAttachData);
        }
    }

    private void showFileNameDialog(String type, String data) {
        FileNameDialogFragment fileNameDialogFragment = new FileNameDialogFragment();
        fileNameDialogFragment.setFileInfo(type, data);
        fileNameDialogFragment.show(getFragmentManager(), "FileNameDialogFragment");
    }

    @Override
    public void onConfirmSaving(String filename, String type, String data) {
        try {
            String extension = "." + MimeTypeMap.getSingleton().getExtensionFromMimeType(type);
            File storageDirectory = Environment.getExternalStorageDirectory();
            byte[] decodedString = Base64.decode(data, Base64.DEFAULT);
            File messengerDirectory = new File(storageDirectory + "/Messenger");
            messengerDirectory.mkdirs();
            filename = filename + extension;
            File file = new File(messengerDirectory + "/" + filename);
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(decodedString, 0, decodedString.length);
            Toast.makeText(MainActivity.this, "File " + filename +
                    " successfully saved to folder Messenger.", Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            Exception ex = e;
            Toast.makeText(MainActivity.this, "An error occurred during file saving.",
                    Toast.LENGTH_LONG).show();
        }
    }
}
