package org.ustimov.messenger.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import org.ustimov.messenger.R;

public class RegisterFragment extends Fragment implements Button.OnClickListener {

    private OnClickListener mListener;
    private EditText mEmailEditText;
    private EditText mPasswordEditText;
    private EditText mNicknameEditText;

    public RegisterFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_register, container, false);

        Button registerButton = (Button) view.findViewById(R.id.registerButton);
        registerButton.setOnClickListener(this);

        mEmailEditText = (EditText) view.findViewById(R.id.registerEmailEditText);
        mPasswordEditText = (EditText) view.findViewById(R.id.registerPasswordEditText);
        mNicknameEditText = (EditText) view.findViewById(R.id.registerNicknameEditText);

        return view;
    }

    @Override
    public void onClick(View v) {
        if (mListener != null) {
            mListener.onRegisterClick(mEmailEditText.getText().toString(),
                    mPasswordEditText.getText().toString(), mNicknameEditText.getText().toString());
            Log.d("REGISTER", "Register clicked!");
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnClickListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement RegisterFragment.OnClickListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnClickListener {
        public void onRegisterClick(String email, String password, String nickname);
    }
}
