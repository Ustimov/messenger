package org.ustimov.messenger.fragments;


import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import org.ustimov.messenger.R;

public class AddContactFragment extends Fragment implements Button.OnClickListener {

    final String TITLE = "Add Contact";

    private OnFragmentInteractionListener mListener;
    private EditText mUserIdEditText;
    private Button mAddContactButton;

    public AddContactFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(TITLE);

        View v = inflater.inflate(R.layout.fragment_add_contact, container, false);

        mUserIdEditText = (EditText) v.findViewById(R.id.userIdEditText);
        mAddContactButton = (Button) v.findViewById(R.id.addContactButton);
        mAddContactButton.setOnClickListener(this);

        return v;
    }

    @Override
    public void onClick(View v) {
        mListener.onAddContactButtonClick(mUserIdEditText.getText().toString());
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement AddContactFragment.OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        public void onAddContactButtonClick(String userId);
    }
}
