package org.ustimov.messenger.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import org.ustimov.messenger.R;

public class LoginFragment extends Fragment implements Button.OnClickListener {

    private OnClickListener mListener;
    private Button mLoginButton;
    private Button mRegisterButton;
    private EditText mLoginEditText;
    private EditText mPasswordEditText;

    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_login, container, false);

        mLoginButton = (Button) view.findViewById(R.id.loginButton);
        mLoginButton.setOnClickListener(this);

        mRegisterButton = (Button) view.findViewById(R.id.loginRegisterButton);
        mRegisterButton.setOnClickListener(this);

        mLoginEditText = (EditText) view.findViewById(R.id.loginEmailEditText);
        mPasswordEditText = (EditText) view.findViewById(R.id.loginPasswordEditText);

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnClickListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement LoginFragment.OnClickListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        if (v == mLoginButton) {
            Log.d("LoginFragment", "Login button is clicked!");
            if (mListener != null) {
                mListener.onLoginClick(ClickedButton.LOGIN, mLoginEditText.getText().toString(),
                        mPasswordEditText.getText().toString());
            }
        } else if (v == mRegisterButton) {
            Log.d("LoginFragment", "Register button is clicked!");
            mListener.onLoginClick(ClickedButton.REGISTER, null, null);
        }
    }

    public interface OnClickListener {
        public void onLoginClick(ClickedButton clickedButton, String login, String password);
    }

    public enum ClickedButton {
        LOGIN, REGISTER
    }
}
