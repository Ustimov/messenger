package org.ustimov.messenger.fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.ustimov.messenger.R;

import java.io.ByteArrayOutputStream;
import java.sql.ResultSet;

public class ProfileFragment extends Fragment implements Button.OnClickListener {

    final int PICK_IMAGE_REQUEST_CODE = 1000;
    final int RESULT_OK = -1;

    private ImageView mUserPictureImageView;

    private TextView mNicknameTextView;
    private EditText mStatusEditText;
    private EditText mEmailEditText;
    private EditText mPhoneEditText;

    private Button mLoadImageButton;
    private Button mSaveProfileButton;

    private String mNickname;
    private String mUserStatus;
    private String mEmail;
    private String mPhone;
    private String mPicture;

    private final String TITLE = "Profile";

    private OnFragmentInteractionListener mListener;

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(TITLE);

        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        mNicknameTextView = (TextView)view.findViewById(R.id.profileNicknameTextView);
        mNicknameTextView.setText(mNickname);

        mUserPictureImageView = (ImageView) view.findViewById(R.id.profileImageView);
        if (mPicture != null && !mPicture.equals("")) {
            byte[] decodedString = Base64.decode(mPicture, Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            mUserPictureImageView.setImageBitmap(decodedByte);
        }

        mStatusEditText = (EditText)view.findViewById(R.id.profileStatusEditText);
        mStatusEditText.setText(mUserStatus);

        mEmailEditText = (EditText) view.findViewById(R.id.profileEmailEditText);
        mEmailEditText.setText(mEmail);

        mPhoneEditText = (EditText) view.findViewById(R.id.profilePhoneEditText);
        mPhoneEditText.setText(mPhone);

        mLoadImageButton = (Button)view.findViewById(R.id.profileLoadImageButton);
        mSaveProfileButton = (Button)view.findViewById(R.id.profileSaveButton);

        mLoadImageButton.setOnClickListener(this);
        mSaveProfileButton.setOnClickListener(this);

        return view;
    }

    public void setUserInfo(String nickname, String userStatus, String email, String phone,
                            String picture) {
        mNickname = nickname;
        mUserStatus = userStatus;
        mEmail = email;
        mPhone = phone;
        mPicture = picture;
    }

    public void updateInfo() {
        mStatusEditText.setText(mUserStatus);
    }

    @Override
    public void onClick(View v) {
        if (v == mLoadImageButton) {
            Log.d("ProfileFragment", "Load image button clicked!");

            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"),
                    PICK_IMAGE_REQUEST_CODE);

        } else if (v == mSaveProfileButton) {
            if (mListener != null) {
                mUserStatus = mStatusEditText.getText().toString();
                mEmail = mEmailEditText.getText().toString();
                mPhone = mPhoneEditText.getText().toString();

                BitmapDrawable drawable = (BitmapDrawable) mUserPictureImageView.getDrawable();
                Bitmap bitmap = drawable.getBitmap();
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                byte[] bytes = byteArrayOutputStream.toByteArray();
                mPicture = Base64.encodeToString(bytes, 0);

                mListener.onSaveButtonClick(mUserStatus, mEmail, mPhone, mPicture);
            }
            Log.d("ProfileFragment", "Save profile button clicked!");
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement LoginFragment.OnClickListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        public void onSaveButtonClick(String userStatus, String email, String phone, String picture);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == PICK_IMAGE_REQUEST_CODE) && (resultCode == RESULT_OK) && (data != null)) {
            Uri uri = data.getData();
            mUserPictureImageView.setImageURI(uri);
        }
    }
}
