package org.ustimov.messenger.fragments.deprecated;

import android.app.Activity;
import android.os.Bundle;
import android.app.ListFragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import org.ustimov.messenger.R;
import org.ustimov.messenger.adapters.ChannelListAdapter;
import org.ustimov.messengerclient.models.Channel;

public class ChannelListFragment extends ListFragment implements Button.OnClickListener {

    private OnFragmentInteractionListener mListener;
    private ChannelListAdapter mAdapter;
    private Button mCreateChannelButton;
    private View mListViewHeader;

    private final String TITLE = "Channels";

    public ChannelListFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(TITLE);

        if (mListener != null) {
            mListener.onChannelListFragmentCreating();
        }

        View view = super.onCreateView(inflater, container, savedInstanceState);

        mListViewHeader = inflater.inflate(R.layout.create_channel_item, container, false);
        mCreateChannelButton = (Button) mListViewHeader.findViewById(R.id.headerCreateChannelButton);
        mCreateChannelButton.setOnClickListener(this);

        return view;
    }

    @Override
    public void onDestroyView() {
        setListAdapter(null);
        super.onDestroyView();

    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        mListener.onChannelClick((Channel) mAdapter.getItem(position));
        Log.d("ChannelListFragment", "Selected chatroom with position" + position);
    }

    public void setAdapter(final ChannelListAdapter adapter) {
        mAdapter = adapter;
        setListAdapter(mAdapter);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement ChannelListFragment.OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        if (mListener != null) {
            mListener.onCreateChannelClick();
        }
        Log.d("ChannelListFragment", "Header clicked!");
    }

    public interface OnFragmentInteractionListener {
        public void onChannelClick(Channel channel);
        public void onCreateChannelClick();
        public void onChannelListFragmentCreating();
    }
}
