package org.ustimov.messenger.fragments.deprecated;

import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import org.ustimov.messenger.R;

public class CreateChannelFragment extends Fragment implements Button.OnClickListener {

    private OnFragmentInteractionListener mListener;

    private EditText mChannelNameEditText;
    private EditText mChannelDescriptionEditText;

    private final String TITLE = "Create Channel";

    public CreateChannelFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(TITLE);

        View view = inflater.inflate(R.layout.fragment_create_channel, container, false);

        Button createChannelButton = (Button)view.findViewById(R.id.createChannelButton);
        createChannelButton.setOnClickListener(this);

        mChannelNameEditText = (EditText)view.findViewById(R.id.createChannelNameEditText);
        mChannelDescriptionEditText = (EditText)view.findViewById(
                R.id.createChannelDescriptionEditText);

        return view;
    }

    @Override
    public void onClick(View v) {
        if (mListener != null) {
            mListener.onCreateButtonClick(mChannelNameEditText.getText().toString(),
                    mChannelDescriptionEditText.getText().toString());
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement ChatFragment.OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        public void onCreateButtonClick(String channelName, String channelDescription);
    }
}
