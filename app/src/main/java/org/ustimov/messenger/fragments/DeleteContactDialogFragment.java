package org.ustimov.messenger.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;

import org.ustimov.messenger.R;
import org.ustimov.messengerclient.models.ContactModel;

/**
 * Created by ustimov on 08/01/16.
 */
public class DeleteContactDialogFragment extends DialogFragment {

    private static ContactModel mContact;

    public interface OnFragmentInteractionListener {
        void onConfirmContactDeleting(String userId);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (!(activity instanceof OnFragmentInteractionListener)) {
            throw new ClassCastException(activity.toString() + " must implement YesNoListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(getActivity())
                .setTitle("Delete contact")
                .setMessage("Are you sure you want to delete contact " + mContact.getNickname() + "?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ((OnFragmentInteractionListener) getActivity())
                                .onConfirmContactDeleting(mContact.getUserId());
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .create();
    }

    public void setContact(ContactModel contact) {
        mContact = contact;
    }
}
