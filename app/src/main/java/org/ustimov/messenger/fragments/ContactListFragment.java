package org.ustimov.messenger.fragments;

import android.app.Activity;
import android.app.ListFragment;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import org.ustimov.messenger.adapters.ContactListAdapter;
import org.ustimov.messengerclient.models.ContactModel;

/**
 * Created by ustimov on 05/01/16.
 */
public class ContactListFragment extends ListFragment implements ListView.OnItemLongClickListener {

    private OnFragmentInteractionListener mListener;
    private ContactListAdapter mAdapter;

    private final String TITLE = "Contacts";

    public ContactListFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(TITLE);
        View view = super.onCreateView(inflater, container, savedInstanceState);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getListView().setOnItemLongClickListener(this);
    }

    @Override
    public void onDestroyView() {
        setListAdapter(null);
        super.onDestroyView();

    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        mListener.onContactClick((ContactModel) mAdapter.getItem(position));
        Log.d("ContactListFragment", "Selected contact with position" + position);
    }

    public void setAdapter(final ContactListAdapter adapter) {
        mAdapter = adapter;
        setListAdapter(mAdapter);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement ContactListFragment.OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void update() {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        ContactModel contact = (ContactModel)mAdapter.getItem(position);
        mListener.onRemoveContact(contact);
        return true;
    }

    public interface OnFragmentInteractionListener {
        public void onRemoveContact(ContactModel contact);
        public void onContactClick(ContactModel contact);
    }
}
