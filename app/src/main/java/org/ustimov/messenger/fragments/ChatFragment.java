package org.ustimov.messenger.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.ustimov.messenger.R;
import org.ustimov.messenger.adapters.ChatListAdapter;
import org.ustimov.messengerclient.models.MessageModel;
import org.ustimov.messengerclient.models.UserType;
import org.ustimov.messengerclient.services.Attach;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.Date;

public class ChatFragment extends Fragment implements AdapterView.OnItemClickListener,
        ImageView.OnClickListener {

    private ListView mChatListView;
    private EditText mChatEditText;
    private ImageView mEnterChatView;
    private ChatListAdapter mAdapter;
    private ImageView mAddAttachImageView;
    private Button mRemoveAttachButton;
    private TextView mAttachTextView;

    private String mAttachData;
    private String mAttachType;

    private OnFragmentInteractionListener mListener;
    private String mNickname;
    private String mContactId;

    public ChatFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(mNickname);

        View view =  inflater.inflate(R.layout.fragment_chat, container, false);

        mChatListView = (ListView) view.findViewById(R.id.chat_list_view);
        mChatEditText = (EditText) view.findViewById(R.id.chat_edit_text1);
        mEnterChatView = (ImageView) view.findViewById(R.id.enter_chat1);
        mRemoveAttachButton = (Button) view.findViewById(R.id.removeAttachButton);
        mAddAttachImageView = (ImageView) view.findViewById(R.id.addAttachImageView);
        mAttachTextView = (TextView) view.findViewById(R.id.attachMessageTextView);

        mRemoveAttachButton.setOnClickListener(this);
        mAddAttachImageView.setOnClickListener(this);

        mChatListView.setOnItemClickListener(this);
        mChatListView.setAdapter(mAdapter);

        mChatEditText.setOnKeyListener(keyListener);
        mEnterChatView.setOnClickListener(clickListener);
        mChatEditText.addTextChangedListener(watcher1);

        return view;
    }

    private final TextWatcher watcher1 = new TextWatcher() {

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            if (mChatEditText.getText().toString().equals("")) {

            } else {
                mEnterChatView.setImageResource(R.drawable.ic_chat_send);
            }
        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (editable.length() == 0) {
                mEnterChatView.setImageResource(R.drawable.ic_chat_send);
            } else {
                //mEnterChatView.setImageResource(R.drawable.ic_chat_send_active);
            }
        }
    };

    private EditText.OnKeyListener keyListener = new View.OnKeyListener() {
        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {

            // If the event is a key-down event on the "enter" button
            if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                // Perform action on key press

                EditText editText = (EditText) v;

                if (v == mChatEditText) {
                    sendMessage(editText.getText().toString(), UserType.OTHER);
                }

                mChatEditText.setText("");

                return true;
            }
            return false;

        }
    };

    private ImageView.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(v == mEnterChatView) {
                sendMessage(mChatEditText.getText().toString(), UserType.OTHER);
            }
            mChatEditText.setText("");
            removeAttachment();
        }
    };

    private void sendMessage(final String messageText, final UserType userType)
    {
        if (messageText.trim().length() == 0) {
            return;
        }

        MessageModel message = new MessageModel();
        message.setText(messageText);
        message.setUserType(UserType.SELF);
        message.setTime(Long.toString(new Date().getTime()));
        Attach attach = new Attach(mAttachType, mAttachData);
        message.setAttach(attach);

        mAdapter.addMessage(message);

        mListener.onMessageSend(messageText, mContactId, attach);

        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
            scrollListViewToBottom();
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement ChatFragment.OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    final int ATTACHMENT_REQUEST_CODE = 0;
    final int RESULT_OK = -1;

    @Override
    public void onClick(View v) {
        if (v == mAddAttachImageView) {
            Intent intent = new Intent();
            intent.setType("*/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(intent, ATTACHMENT_REQUEST_CODE);
        } else if (v == mRemoveAttachButton) {
            removeAttachment();
        }
    }

    private void removeAttachment() {
        mAttachTextView.setVisibility(View.GONE);
        mRemoveAttachButton.setVisibility(View.GONE);
        mAttachType = null;
        mAttachData = null;
    }

    public String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension.toLowerCase());
        }
        return type;
    }

    final int READ_EXTERNAL_PERMISSION_REQUEST_RESULT = 0;
    Intent mData;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == ATTACHMENT_REQUEST_CODE) && (resultCode == RESULT_OK) && (data != null)) {
            mData = data;
            if (ContextCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    Toast.makeText(getActivity(), "Attaching files requires read external storage" +
                            " permission!", Toast.LENGTH_LONG).show();
                } else {
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{ Manifest.permission.READ_EXTERNAL_STORAGE },
                            READ_EXTERNAL_PERMISSION_REQUEST_RESULT);
                }
            } else {
                loadAttachment(data);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[],
                                           int[] grantResults) {
        switch (requestCode) {
            case READ_EXTERNAL_PERMISSION_REQUEST_RESULT: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    loadAttachment(mData);
                }
                break;
            }
        }
    }

    private void loadAttachment(Intent data) {
        Uri uri = data.getData();
        File storageDirectory = Environment.getExternalStorageDirectory();
        String path = uri.getPath();
        if (path.contains(":")) {
            String[] splits = path.split(":");
            path = splits[splits.length - 1];
        }
        File file;
        if (path.contains(storageDirectory + "")) {
            file = new File(path);
        } else {
            file = new File(storageDirectory + "/" + path);
        }
        try {
            BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(file));
            int length = (int)file.length();
            byte[] bytes = new byte[length];
            bufferedInputStream.read(bytes, 0, length);
            mAttachType = getMimeType(uri.getPath());
            mAttachData = Base64.encodeToString(bytes, Base64.DEFAULT);
            mAttachTextView.setText("Attached file: " + file.getName());
            mRemoveAttachButton.setVisibility(View.VISIBLE);
            mAttachTextView.setVisibility(View.VISIBLE);
            Toast.makeText(getActivity(), "File successfully attached", Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            Log.d("ATTACHMENT", "Exception during attachment");
            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public interface OnFragmentInteractionListener {
        public void onMessageSend(String message, String channelId, Attach attach);
        public void onUserClick(String userId);
    }

    @Override
    public void onItemClick(AdapterView parent, View view, int position, long id) {
        if (mListener != null) {
            MessageModel message = (MessageModel)parent.getItemAtPosition(position);
            String userId = message.getUserId();
            if (userId == null) {
                return;
            }
            mListener.onUserClick(userId);
        }
    }

    private void scrollListViewToBottom() {
        mChatListView.post(new Runnable() {
            @Override
            public void run() {
                mChatListView.setSelection(mAdapter.getCount() - 1);
            }
        });
    }

    public void setAdapter(final ChatListAdapter adapter) {
        mAdapter = adapter;
    }

    public void setContactId(String contactId) {
        mContactId = contactId;
    }

    public void setContactNickname(String nickname) {
        mNickname = nickname;
    }

    public void update() {
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
            scrollListViewToBottom();
        }
    }
}
