package org.ustimov.messenger.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.widget.EditText;

/**
 * Created by ustimov on 17/01/16.
 */
public class FileNameDialogFragment extends DialogFragment {

    private static String mAttachType;
    private static String mAttachData;

    public interface OnFragmentInteractionListener {
        void onConfirmSaving(String filename, String type, String data);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (!(activity instanceof OnFragmentInteractionListener)) {
            throw new ClassCastException(activity.toString() +
                    " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final EditText input = new EditText(getActivity());
        input.setInputType(InputType.TYPE_CLASS_TEXT);

        return new AlertDialog.Builder(getActivity())
                .setTitle("Input filename")
                .setView(input)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ((OnFragmentInteractionListener) getActivity())
                                .onConfirmSaving(input.getText().toString(), mAttachType,
                                        mAttachData);
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .create();
    }

    public void setFileInfo(String type, String data) {
        mAttachType = type;
        mAttachData = data;
    }
}
