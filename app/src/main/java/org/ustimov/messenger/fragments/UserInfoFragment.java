package org.ustimov.messenger.fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.ustimov.messenger.R;

public class UserInfoFragment extends Fragment {

    /*
    private TextView mNickname;
    private TextView mStatus;
    */
    private final String TITLE = "User Info";

    private static String mNickname;
    private static String mStatus;
    private static String mEmail;
    private static String mPhone;
    private static String mPicture;

    public UserInfoFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(TITLE);

        View view = inflater.inflate(R.layout.fragment_user_info, container, false);

        TextView nickname = (TextView)view.findViewById(R.id.userInfoNicknameTextView);
        nickname.setText(mNickname);


        TextView status = (TextView)view.findViewById(R.id.userInfoStatusTextView);
        status.setText(mStatus);

        TextView email = (TextView)view.findViewById(R.id.userInfoEmailTextView);
        email.setText(mEmail);

        TextView phone = (TextView)view.findViewById(R.id.userInfoPhoneTextView);
        phone.setText(mPhone);

        ImageView picture = (ImageView)view.findViewById(R.id.userInfoImageView);
        byte[] decodedString = Base64.decode(mPicture, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        picture.setImageBitmap(decodedByte);

        return view;
    }

    public void setUserInfo(String nickname, String status, String email, String phone,
                            String picture) {
        mNickname = nickname;
        mStatus = status;
        mEmail = email;
        mPhone = phone;
        mPicture = picture;
    }

}
