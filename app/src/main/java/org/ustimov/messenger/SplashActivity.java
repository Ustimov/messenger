package org.ustimov.messenger;

import android.os.Bundle;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import org.ustimov.messenger.fragments.ImageFragment;
import org.ustimov.messenger.fragments.LoadingFragment;
import org.ustimov.messenger.fragments.LoginFragment;
import org.ustimov.messenger.fragments.RegisterFragment;
import org.ustimov.messenger.services.MessagingService;
import org.ustimov.messenger.services.MessagingServiceConnection;
import org.ustimov.messenger.services.SessionManager;
import org.ustimov.messengerclient.events.IEvent;
import org.ustimov.messengerclient.events.TimeoutEvent;
import org.ustimov.messengerclient.events.WelcomeEvent;
import org.ustimov.messengerclient.MessengerClient;
import org.ustimov.messengerclient.responses.AuthResponse;
import org.ustimov.messengerclient.responses.IResponse;
import org.ustimov.messengerclient.responses.RegisterResponse;

public class SplashActivity extends BaseActivity implements
        LoginFragment.OnClickListener, RegisterFragment.OnClickListener,
        MessengerClient.EventListener, MessengerClient.ResponseListener,
        MessagingServiceConnection.OnConnectListener {

    private boolean mIsFirstLoad = true;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        SessionManager.onlySettings = false;

        Intent intent = new Intent(this, MessagingService.class);

        if (savedInstanceState == null) {
            AdvancedFragmentManager.changeFragment(this, R.id.fragment_container_splash,
                    new ImageFragment(), false, null);
            startService(intent);
            mIsFirstLoad = false;
        }

        mServiceConnection = new MessagingServiceConnection(this);
        bindService(intent, mServiceConnection, 0);
        mServiceConnection.setBounded(true);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onConnect() {
        mMessagingService = mServiceConnection.getMessagingService();
        mMessagingService.addEventListener(this);
        mMessagingService.connect();
    }

    @Override
    public void onEvent(final IEvent event) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (event instanceof WelcomeEvent) {
                    processWelcomeEvent((WelcomeEvent) event);
                } else if (event instanceof TimeoutEvent) {
                    Toast.makeText(SplashActivity.this, "Internet connection error",
                            Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //SplashActivity.this.finish();
                            SessionManager.onlySettings = true;
                            startMainActivity();
                        }
                    }, 2000);
                }
                Log.d("SplashActivity", "Event is come");
            }
        });
    }

    @Override
    public void onResponse(final IResponse response) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (response instanceof AuthResponse) {
                    getFragmentManager().popBackStack();
                    processAuthResponse((AuthResponse) response);
                }
                if (response instanceof RegisterResponse) {
                    getFragmentManager().popBackStack();
                    processRegisterResponse((RegisterResponse) response);
                }
                Log.d("SplashActivity", "Response is come");
            }
        });
        if (!response.getStatus().equals("0")) {
            SessionManager.setAuthData(this, null, null);
            SessionManager.setCredenals(this, null, null);
            mMessagingService.connect();
        }
    }

    @Override
    public void onRegisterClick(String email, String password, String nickname) {
        AdvancedFragmentManager.changeFragment(this, R.id.fragment_container_splash,
                new LoadingFragment(), true, null);
        SessionManager.setAuthData(this, email, password);
        mMessagingService.sendRegisterRequest(this, email, password, nickname);
        Log.d("SplashActivity", "Register request was sent");
    }

    @Override
    public void onLoginClick(LoginFragment.ClickedButton clickedButton, String login, String password) {
        if (clickedButton == LoginFragment.ClickedButton.LOGIN) {
            SessionManager.setAuthData(this, login, password);
            AdvancedFragmentManager.changeFragment(this, R.id.fragment_container_splash,
                    new LoadingFragment(), true, null);
            mMessagingService.sendAuthRequest(this, login, password);
            Log.d("SplashActivity", "Auth request was sent");
        } else if (clickedButton == LoginFragment.ClickedButton.REGISTER) {
            AdvancedFragmentManager.changeFragment(this, R.id.fragment_container_splash,
                    new RegisterFragment(), true, null);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        if(mServiceConnection.isBounded()) {
            mMessagingService.removeEventListener(this);
            unbindService(mServiceConnection);
            mServiceConnection.setBounded(false);
        }
    }

    private void showLoginFragment() {
        AdvancedFragmentManager.changeFragment(this, R.id.fragment_container_splash,
                new LoginFragment(), false, null);
    }

    private void startMainActivity() {
        Intent mainIntent = new Intent(SplashActivity.this, MainActivity.class);
        mainIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        SplashActivity.this.startActivity(mainIntent);
        SplashActivity.this.finish();
    }

    /* region Processing */
    private void processWelcomeEvent(WelcomeEvent event) {
        Toast.makeText(SplashActivity.this, "Welcome", Toast.LENGTH_SHORT).show();

        String email = SessionManager.getEmail(this);
        String password = SessionManager.getPassword(this);
        if (email == null || password == null) {
            showLoginFragment();
        } else {
            mMessagingService.sendAuthRequest(this, email, password);
        }

    }

    private void processRegisterResponse(final RegisterResponse response) {
        Toast.makeText(SplashActivity.this, response.getError(), Toast.LENGTH_SHORT).show();
    }

    private void processAuthResponse(final AuthResponse response) {
        SessionManager.setCredenals(this, response.getUserId(), response.getSessionId());

        Toast.makeText(SplashActivity.this, response.getError(), Toast.LENGTH_SHORT).show();
        if (response.getStatus().equals("0")) {
            startMainActivity();
        }
    }
    /* end region */
}
