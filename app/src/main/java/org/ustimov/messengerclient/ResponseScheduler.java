package org.ustimov.messengerclient;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by ustimov on 22/11/15.
 */
public class ResponseScheduler {
    private Timer mTimer;
    private String mAction;
    private OnTimeoutListener mListener;

    public void schedule(OnTimeoutListener listener, String action, int milliseconds) {
        //stop(mAction);
        mListener = listener;
        mAction = action;
        mTimer = new Timer();
        mTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (mListener != null) {
                    mListener.onTimeout();
                }
            }
        }, milliseconds);
    }

    public void stop(String action) {
        if (mAction.equals(action)) {
            try {
                mTimer.cancel();
            } catch (Exception e) {

            }

        }
    }

    public interface OnTimeoutListener {
        public void onTimeout();
    }
}
