package org.ustimov.messengerclient.models;

import java.util.ArrayList;

/**
 * Created by ustimov on 06/01/16.
 */
public class ContactModel {
    private ArrayList<MessageModel> mMessages = new ArrayList<>();
    private String mUserId;
    private String mNickname;
    private String mEmail;
    private String mPhone;
    private String mPicture;

    public ContactModel() {

    }

    public ContactModel(String userId, String nickname, String email, String phone, String picture) {
        mUserId = userId;
        mNickname = nickname;
        mEmail = email;
        mPhone = phone;
        mPicture = picture;
    }

    public ArrayList<MessageModel> getMessages() {
        return  mMessages;
    }

    public String getUserId() {
        return mUserId;
    }

    public String getNickname() {
        return mNickname;
    }

    public String getEmail() {
        return mEmail;
    }

    public String getPhone() {
        return mPhone;
    }

    public String getPicture() {
        return mPicture;
    }

    public void setUserId(String userId) {
        mUserId = userId;
    }

    public void setNickname(String nickname) {
        mNickname = nickname;
    }

    public void addMessage(MessageModel message) {
        mMessages.add(message);
    }

    public String getLastMessage() {
        if (mMessages.size() == 0) {
            return "There are no messages.";
        }
        return mMessages.get(mMessages.size() - 1).getText();
    }
}
