package org.ustimov.messengerclient.models;

/**
 * Created by ustimov on 05/01/16.
 */
public class ResponseContact {
    public String myid;
    public String uid;
    public String nick;
    public String email;
    public String phone;
    public String picture;

    public String getUserId() {
        return uid;
    }

    public String getNickname() {
        return nick;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getPicture() {
        return picture;
    }
}
