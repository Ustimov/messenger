package org.ustimov.messengerclient.models;

import java.util.ArrayList;

/**
 * Created by ustimov on 06/01/16.
 */
public class Storage {
    private static ArrayList<ContactModel> mContacts = new ArrayList<>();

    public static ArrayList<ContactModel> getContacts() {
        return mContacts;
    }

    public static void setContacts(ResponseContact[] contacts) {
        boolean exist = false;
        for (ResponseContact contact : contacts) {
            for (ContactModel c : mContacts) {
                if (contact.getUserId().equals(c.getUserId())) {
                    exist = true;
                    break;
                }
            }
            if (!exist) {
                mContacts.add(new ContactModel(contact.getUserId(), contact.getNickname(),
                        contact.getEmail(), contact.getPhone(), contact.getPicture()));
            } else {
                exist = false;
            }
        }
    }

    public static void addMessage(MessageModel message) {
        for (ContactModel contact : mContacts) {
            if (contact.getUserId().equals(message.getUserId())) {
                contact.addMessage(message);
                return;
            }
        }
        // Todo: consistency
        ContactModel contact = new ContactModel();
        contact.setUserId(message.getUserId());
        contact.setNickname(message.getNickname());
        contact.addMessage(message);
        mContacts.add(contact);
    }

    public static ContactModel getContact(String userId) {
        for (ContactModel contact : mContacts) {
            if (contact.getUserId().equals(userId)) {
                return contact;
            }
        }
        return null;
    }

    public static void deleteContact(String userId) {
        ContactModel found = null;
        for (ContactModel contact : mContacts) {
            if (contact.getUserId().equals(userId)) {
                found = contact;
                break;
            }
        }
        if (found != null) {
            mContacts.remove(found);
        }
    }

    public static void clear() {
        mContacts = new ArrayList<>();
    }
}
