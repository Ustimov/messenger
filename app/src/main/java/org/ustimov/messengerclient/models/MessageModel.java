package org.ustimov.messengerclient.models;

import org.ustimov.messengerclient.services.Attach;

import java.text.AttributedCharacterIterator;

/**
 * Created by ustimov on 06/01/16.
 */
public class MessageModel {
    private UserType mUsetType;
    private String mText;
    private String mNickname;
    private String mTime;
    private String mUserId;
    private Attach mAttach;

    public MessageModel() {

    }

    public MessageModel(String userId, String nickname, String text, String time, Attach attach,
                        UserType userType) {
        mUserId = userId;
        mNickname = nickname;
        mText = text;
        mTime = time;
        mAttach = attach;
        mUsetType = userType;
    }

    public UserType getUserType() {
        return mUsetType;
    }

    public String getText() {
        return mText;
    }

    public String getNickname() {
        return mNickname;
    }

    public String getTime() {
        return mTime;
    }

    public String getUserId() {
        return mUserId;
    }

    public Attach getAttach() { return mAttach; }

    public void setUserType(UserType userType) {
        mUsetType = userType;
    }

    public void setText(String text) {
        mText = text;
    }

    public void setNickname(String nickname) {
        mNickname = nickname;
    }

    public void setTime(String time) {
        mTime = time;
    }

    public void setUserId(String userId) {
        mUserId = userId;
    }

    public void setAttach(Attach attach) {
        mAttach = attach;
    }
}
