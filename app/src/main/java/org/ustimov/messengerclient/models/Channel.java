package org.ustimov.messengerclient.models;

/**
 * Created by ustimov on 14/11/15.
 */
public class Channel {
    public String chid;
    public String name;
    public String descr;
    public String online;

    public String getId() {
        return chid;
    }

    public void setId(String channelId) {
        chid = channelId;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return descr;
    }

    public String getFormatedOnline() {
        return "(" + online + ")";
    }
}
