package org.ustimov.messengerclient.models;

/**
 * Created by ustimov on 14/11/15.
 */
public class User {
    public String uid;
    public String nick;
    public String email;
    public String phone;
}
