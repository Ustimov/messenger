package org.ustimov.messengerclient.models;

import java.util.Date;

/**
 * Created by ustimov on 14/11/15.
 */
public class Message {
    protected transient UserType mUserType;
    public String mid;
    public String from;
    public String nick;
    public String body;
    public String time;

    public UserType getUserType() {
        return mUserType;
    }

    public void setUserType(UserType userType) {
        mUserType = userType;
    }

    public long getTime() {
        return new Date().getTime();
    }

    public String getText() {
        return body;
    }

    public String getUserName() {
        return  nick;
    }

    public void setText(String message) {
        body = message;
    }

    public void setTime(long time) {

    }

    public String getUserId() {
        return from;
    }
}
