package org.ustimov.messengerclient.models;

/**
 * Created by ustimov on 05/01/16.
 */
public class RequestContact {
    public String myid;
    public String name;
    public String phone;
    public String email;

    public void setUserId(String userId) {
        myid = userId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
