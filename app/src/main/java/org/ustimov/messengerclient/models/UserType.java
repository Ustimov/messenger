package org.ustimov.messengerclient.models;

public enum UserType {
    OTHER, SELF
};