package org.ustimov.messengerclient;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import org.ustimov.messengerclient.events.IEvent;
import org.ustimov.messengerclient.events.TimeoutEvent;
import org.ustimov.messengerclient.requests.IRequest;
import org.ustimov.messengerclient.events.WelcomeEvent;
import org.ustimov.messengerclient.responses.IResponse;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by ustimov on 13/11/15.
 */
public class MessengerClient implements ResponseScheduler.OnTimeoutListener {

    private final int SLEEP_TIME = 1000;
    private final int RESPONSE_TIMEOUT = 10000;

    private String mHost;
    private int mPort;
    private Socket mSocket;
    private ResponseListener mCurrentListener;
    private ArrayList<EventListener> mEventListeners = new ArrayList<>();
    private Queue<RequestPair> mRequestQueue = new ConcurrentLinkedQueue<>();
    private Gson mGson = new Gson();
    private ResponseScheduler mResponseSheduler = new ResponseScheduler();

    private BufferedOutputStream mOutputStream;
    private BufferedInputStream mInputStream;

    public MessengerClient(String host, int port) {
        mHost = host;
        mPort = port;
    }

    public void connect() throws IOException{
        mResponseSheduler.schedule(this, Actions.WELCOME, RESPONSE_TIMEOUT);

        mSocket = new Socket(mHost, mPort);
        mOutputStream = new BufferedOutputStream(mSocket.getOutputStream());
        mInputStream = new BufferedInputStream(mSocket.getInputStream());

        new Thread(new Listener()).start();
        new Thread(new Sender()).start();
    }

    public void send(IRequest request, ResponseListener listener) {
        mRequestQueue.add(new RequestPair(request, listener));
    }

    public void addEventListener(EventListener listener) {
        mEventListeners.add(listener);
    }

    public void removeEventListener(EventListener listener) {
        mEventListeners.remove(listener);
    }

    @Override
    public void onTimeout() {
        TimeoutEvent timeoutEvent = new TimeoutEvent();
        for (EventListener listener : mEventListeners) {
            listener.onEvent(timeoutEvent);
        }
    }

    public interface ResponseListener {
        public void onResponse(IResponse response);
    }

    public interface EventListener {
        public void onEvent(IEvent event);
    }

    private class Listener implements Runnable {

        @Override
        public void run() {
            byte[] data = new byte[32768];
            int offset = 0;
            boolean cleanup = false;
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            while (true) {
                try {
                    if (cleanup) {
                        byteArrayOutputStream.reset();
                        cleanup = false;
                    }

                    int readBytes = mInputStream.read(data);
                    if (readBytes != -1) {
                        byteArrayOutputStream.write(data, 0, readBytes);
                        byteArrayOutputStream.flush();
                        String result = byteArrayOutputStream.toString("utf-8");
                        if (result.endsWith("}")) {
                            try {
                                String[] responses = result.split("\\}\\{");
                                for (int i = 0; i < responses.length; i++) {
                                    String json = responses[i];
                                    if (i != 0) {
                                        json = "{" + json;
                                    }
                                    if (i != responses.length - 1) {
                                        json += "}";
                                    }
                                    processInput(json);
                                }
                                cleanup = true;
                            } catch (JsonSyntaxException e) {
                                //not full json, continue
                                System.out.println(result);
                            }
                        }
                    }
                } catch (IOException e) {
                    System.out.println("[EXCEPTION] Listener read");
                }
            }
        }

        private void processInput(String json) {
            ResponseFrame responseFrame = mGson.fromJson(json, ResponseFrame.class);
            String action = responseFrame.getAction();
            if (action == null) {
                return;
            }
            if (Actions.isEvent(action)) {
                IEvent event;
                if (action.equals(Actions.WELCOME)) {
                    event = mGson.fromJson(json, WelcomeEvent.class);
                } else {
                    event = Actions.getEvent(action, responseFrame.getData());
                }
                for (EventListener listener : mEventListeners) {
                    listener.onEvent(event);
                }
            } else {
                IResponse response = Actions.getResponse(action, responseFrame.getData());
                if (mCurrentListener != null) {
                    mCurrentListener.onResponse(response);
                }
            }
            mResponseSheduler.stop(action);
        }
    }

    private class Sender implements Runnable {

        @Override
        public void run() {
            while (true) {
                if (mRequestQueue.size() == 0) {
                    try {
                        Thread.sleep(SLEEP_TIME);
                        continue;
                    } catch (InterruptedException e) {
                        System.out.println("[EXCEPTION] Sender interrupted");
                    }
                }
                RequestPair requestPair = mRequestQueue.poll();
                ResponseListener listener = requestPair.getListener();
                if (listener == null) {
                    continue;
                }
                mCurrentListener = listener;
                IRequest request = requestPair.getRequest();

                String action = request.getAction();
                mResponseSheduler.schedule(MessengerClient.this, action, RESPONSE_TIMEOUT);

                RequestFrame requestFrame = new RequestFrame(action, request);
                String requestString = mGson.toJson(requestFrame);
                byte[] data = requestString.getBytes(Charset.forName("UTF-8"));
                try {
                    mOutputStream.write(data);
                    mOutputStream.flush();
                } catch (IOException e) {
                    System.out.println("[EXCEPTION] Sender write");
                }
            }
        }
    }

    private class RequestPair {

        private IRequest mRequest;
        private ResponseListener mListener;

        public RequestPair(IRequest request, ResponseListener listener) {
            mRequest = request;
            mListener = listener;
        }

        public IRequest getRequest() {
            return mRequest;
        }

        public ResponseListener getListener() {
            return mListener;
        }
    }
}
