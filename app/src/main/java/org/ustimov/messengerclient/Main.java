import org.ustimov.messengerclient.MessengerClient;
import org.ustimov.messengerclient.events.*;
import org.ustimov.messengerclient.events.deprecated.ChatRoomEnterEvent;
import org.ustimov.messengerclient.events.deprecated.ChatRoomLeaveEvent;
import org.ustimov.messengerclient.models.Channel;
import org.ustimov.messengerclient.models.Message;
import org.ustimov.messengerclient.models.User;
import org.ustimov.messengerclient.requests.*;
import org.ustimov.messengerclient.requests.deprecated.ChannelListRequest;
import org.ustimov.messengerclient.requests.deprecated.EnterChannelRequest;
import org.ustimov.messengerclient.requests.deprecated.LeaveChannelRequest;
import org.ustimov.messengerclient.responses.*;
import org.ustimov.messengerclient.responses.depricated.ChannelListResponse;
import org.ustimov.messengerclient.responses.depricated.EnterChannelResponse;
import org.ustimov.messengerclient.responses.depricated.LeaveChannelResponse;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by ustimov on 13/11/15.
 */
public class Main {

    public static final String HOST = "188.166.49.215";
    public static final int PORT = 7777;

    public static String md5(String s) {
        String md5sum = null;
        try {
            MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            StringBuffer hexString = new StringBuffer();
            for (int i=0; i<messageDigest.length; i++)
                hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
            md5sum = hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return md5sum;
    }

    public static void main(String args[]) {


        //Gson gson = new Gson();

        //String json = "{\"action\":\"register\",\"data\":{\"status\":0,\"error\":\"OK\"}}{\"action\":\"auth\",\"data\":{\"sid\":\"19c3c11f00a2128954c479656a36b30b\",\"cid\":\"plushkin\",\"status\":0,\"error\":\"OK\"}}";

        //String json = "{\"action\":\"register\",\"data\":{\"status\":0,\"error\":\"OK\"}}";

        //JsonElement jsonElement = gson.toJsonTree(json);

        //JsonObject jsonObject = jsonElement.getAsJsonObject();
        /*
        String[] s = json.split("\\}\\{");
        for (int i = 0; i < s.length; i++) {
            String str = s[i];
            if (i != 0) {
                str = "{" + str;
            }
            if (i != s.length - 1) {
                str += "}";
            }
            System.out.println(str);
        }

        */

        //Type type = new TypeToken<ResponseFrame>() {}.getType();

        //ResponseFrame responseFrame = gson.fromJson(json, ResponseFrame.class);

        //System.out.println(responseFrame.action);
        //return;

        //JsonElement element = .parseValue();


        final MessengerClient messangerClient = new MessengerClient(HOST, PORT);
        messangerClient.addEventListener(new MessengerClient.EventListener() {
            @Override
            public void onEvent(IEvent event) {
                if (event instanceof WelcomeEvent) {

                    WelcomeEvent welcomeEvent = (WelcomeEvent) event;
                    System.out.println(welcomeEvent.getAction());

                    /*
                    messangerClient.send(new RegisterRequest("plushkin2", md5("12345"), "plushkin2"),
                            new MessengerClient.ResponseListener() {
                                @Override
                                public void onResponse(IResponse response) {
                                    if (response instanceof RegisterResponse) {
                                        System.out.println("Register response");
                                    }
                                    if (response instanceof AuthResponse) {
                                        System.out.println("Auth response");
                                    }
                                    System.out.println(response.getError());
                                    System.out.println(response.getStatus());
                                }
                            });
                    */
                    messangerClient.send(new AuthRequest("plushkin1", md5("12345")),
                            new MessengerClient.ResponseListener() {

                                String mUserId;
                                String mSessionId;
                                String mChannelId;

                                @Override
                                public void onResponse(IResponse response) {
                                    if (response instanceof AuthResponse) {
                                        ChannelListRequest channelListRequest = new ChannelListRequest();

                                        mUserId = ((AuthResponse) response).getUserId();
                                        mSessionId = ((AuthResponse) response).getSessionId();

                                        channelListRequest.setAuthData(mUserId, mSessionId);

                                        messangerClient.send(channelListRequest, this);
                                    }
                                    if (response instanceof ChannelListResponse) {
                                        for (Channel c : ((ChannelListResponse) response).channels) {
                                            System.out.println(c.name);
                                            mChannelId = c.chid;
                                        }
                                        //ChatRoomEnterEvent chatRoomEnterEvent = new ChatRoomEnterEvent(mChannelId);
                                        EnterChannelRequest enterChannelRequest = new EnterChannelRequest(mChannelId);
                                        enterChannelRequest.setAuthData(mUserId, mSessionId);
                                        messangerClient.send(enterChannelRequest, this);
                                    }
                                    if (response instanceof EnterChannelResponse) {
                                        for (User user : ((EnterChannelResponse) response).users) {
                                            System.out.println(user.nick);
                                        }
                                        for (Message msg : ((EnterChannelResponse) response).last_msg) {
                                            System.out.println(msg.nick + " " + msg.body);
                                        }

                                        /*
                                        SendMessageRequest sendMsgToChatRoomRequest =
                                                new SendMessageRequest(mChannelId, "Ustimov was here");
                                        sendMsgToChatRoomRequest.setAuthData(mUserId, mSessionId);
                                        messangerClient.send(sendMsgToChatRoomRequest, this);
                                        */

                                        LeaveChannelRequest leaveChannelRequest = new LeaveChannelRequest(mChannelId);
                                        leaveChannelRequest.setAuthData(mUserId, mSessionId);

                                        messangerClient.send(leaveChannelRequest, this);

                                    }
                                    if (response instanceof LeaveChannelResponse) {
                                        System.out.println("channel left");
                                    }

                                    System.out.println(response.getError());
                                    System.out.println(response.getStatus());
                                }
                            });
                } else if (event instanceof ChatRoomEnterEvent) {
                    System.out.println(((ChatRoomEnterEvent)event).nick + " enter chat");
                } else if (event instanceof ChatRoomLeaveEvent) {
                    System.out.println(((ChatRoomLeaveEvent)event).nick + " leave chat");
                } else if (event instanceof SentMessageEvent) {
                    System.out.println(((SentMessageEvent)event).nick + " say " + ((SentMessageEvent)event).body);
                }
                else {
                    System.out.println("Unrecognized event");
                }
            }
        });
        try {
            messangerClient.connect();
        } catch (IOException e) {
            System.out.println("Connection error");
        }

        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(10);
                    } catch (Exception e) {

                    }
                }
            }
        });
        t.start();
        try {
            t.join();
        } catch (Exception e) {
            System.out.println("Can't join");
        }


       /*
        Gson gson = new Gson();
        //AuthRequest authRequest =
        //org.ustimov.messenger.services.requests.AuthRequest authRequest = new org.ustimov.messenger.services.requests.AuthRequest();
        //String json = gson.toJson(authRequest);


        IRequest request = new AuthRequest("This is login", "This is pass");
        String json = gson.toJson(request);
        Frame frame = new Frame(request.getAction(), json);
        String requestString = gson.toJson(frame);
        //AuthRequest authRequest1 = gson.fromJson(json, AuthRequest.class);

        System.out.println(requestString);

        WelcomeEvent welcomeEvent = new WelcomeEvent();
        welcomeEvent.action = "welcome";
        welcomeEvent.message = "qq";
        welcomeEvent.time = "time";

        String json = gson.toJson(welcomeEvent);

        Frame frame = gson.fromJson(json, Frame.class);

        System.out.println(frame.action);
        */
    }

}
