package org.ustimov.messengerclient;

import org.ustimov.messengerclient.responses.ResponseAggregator;

/**
 * Created by ustimov on 13/11/15.
 */
public class ResponseFrame extends Frame {

    public ResponseAggregator data;

    public ResponseFrame(String action, ResponseAggregator data) {
        super();
        this.data = data;
    }

    public ResponseAggregator getData() {
        return data;
    }
}
