package org.ustimov.messengerclient.events;

/**
 * Created by ustimov on 08/11/15.
 */
public class WelcomeEvent implements IEvent {
    public  String action;
    public String message;
    public String time;

    public String getAction() {
        return action;
    }
}
