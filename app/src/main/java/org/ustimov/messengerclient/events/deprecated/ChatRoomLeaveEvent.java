package org.ustimov.messengerclient.events.deprecated;

import org.ustimov.messengerclient.events.IEvent;
import org.ustimov.messengerclient.responses.ResponseAggregator;

/**
 * Created by ustimov on 08/11/15.
 */
public class ChatRoomLeaveEvent extends ResponseAggregator implements IEvent {
    public ChatRoomLeaveEvent(String channelId, String userId, String nickname) {
        super();
        chid = channelId;
        uid = userId;
        nick = nickname;
    }
}
