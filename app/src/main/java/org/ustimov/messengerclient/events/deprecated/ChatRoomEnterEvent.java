package org.ustimov.messengerclient.events.deprecated;

import org.ustimov.messengerclient.events.IEvent;
import org.ustimov.messengerclient.responses.ResponseAggregator;

/**
 * Created by ustimov on 08/11/15.
 */
public class ChatRoomEnterEvent extends ResponseAggregator implements IEvent {
    public ChatRoomEnterEvent(String channelId, String userId, String nickname) {
        super();
        this.chid = channelId;
        this.uid = userId;
        this.nick = nickname;
    }
}
