package org.ustimov.messengerclient.events;

import org.ustimov.messengerclient.responses.ResponseAggregator;
import org.ustimov.messengerclient.services.Attach;

/**
 * Created by ustimov on 08/11/15.
 */
public class SentMessageEvent extends ResponseAggregator implements IEvent {
    public SentMessageEvent(String fromUserId, String nickname,
                            String message, String time, Attach attach) {
        super();
        this.from = fromUserId;
        this.nick = nickname;
        this.body = message;
        this.time = time;
        this.attach = attach;
    }

    public String getUserId() {
        return from;
    }

    public String getNickname() {
        return nick;
    }

    public String getText() {
        return body;
    }

    public String getTime() {
        return time;
    }

    public Attach getAttach() {
        return attach;
    }
}
