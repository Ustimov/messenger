package org.ustimov.messengerclient;

/**
 * Created by ustimov on 08/11/15.
 */
public abstract class AuthData {
    public String cid;
    public String sid;

    public void setAuthData(String userId, String sessionId) {
        cid = userId;
        sid = sessionId;
    }
}
