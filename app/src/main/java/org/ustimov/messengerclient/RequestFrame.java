package org.ustimov.messengerclient;

import org.ustimov.messengerclient.requests.IRequest;

/**
 * Created by ustimov on 13/11/15.
 */
public class RequestFrame extends Frame {

    public IRequest data;

    public RequestFrame(String action, IRequest request) {
        super();
        this.action = action;
        this.data = request;
    }


    public IRequest getData() {
        return data;
    }
}
