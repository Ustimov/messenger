package org.ustimov.messengerclient.responses;

import org.ustimov.messengerclient.models.ResponseContact;

/**
 * Created by ustimov on 05/01/16.
 */
public class ImportContactsResponse extends ContactListResponse {
    public ImportContactsResponse(String status, String error, ResponseContact[] contacts) {
        super(status, error, contacts);
    }
}
