package org.ustimov.messengerclient.responses;

/**
 * Created by ustimov on 05/01/16.
 */
public class DeleteContactResponse extends ResponseAggregator {
    public DeleteContactResponse(String status, String error, String userId) {
        super(status, error);
        uid = userId;
    }

    public String getUserId() {
        return cid;
    }

    public String getSessionId() {
        return sid;
    }
}
