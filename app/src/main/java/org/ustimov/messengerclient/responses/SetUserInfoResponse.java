package org.ustimov.messengerclient.responses;

/**
 * Created by ustimov on 22/11/15.
 */
public class SetUserInfoResponse extends ResponseAggregator {
    public SetUserInfoResponse(String status, String error) {
        super(status, error);
    }
}
