package org.ustimov.messengerclient.responses.depricated;

import org.ustimov.messengerclient.models.Channel;
import org.ustimov.messengerclient.responses.ResponseAggregator;

/**
 * Created by ustimov on 08/11/15.
 */
public class ChannelListResponse extends ResponseAggregator {
    public ChannelListResponse(String status, String error, Channel[] channels) {
        super(status, error);
        this.channels = channels;
    }

    public Channel[] getChannels() {
        return channels;
    }
}
