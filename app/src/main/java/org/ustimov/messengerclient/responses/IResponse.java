package org.ustimov.messengerclient.responses;

/**
 * Created by ustimov on 14/11/15.
 */
public interface IResponse {
    public String getStatus();
    public String getError();
}
