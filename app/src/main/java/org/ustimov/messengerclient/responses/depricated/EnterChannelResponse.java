package org.ustimov.messengerclient.responses.depricated;

import org.ustimov.messengerclient.models.Message;
import org.ustimov.messengerclient.models.User;
import org.ustimov.messengerclient.responses.ResponseAggregator;

/**
 * Created by ustimov on 08/11/15.
 */
public class EnterChannelResponse extends ResponseAggregator {
    public EnterChannelResponse(String status, String error, User[] users, Message[] messages) {
        super(status, error);
        this.users = users;
        this.last_msg = messages;
    }
}
