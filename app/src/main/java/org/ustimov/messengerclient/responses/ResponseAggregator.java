package org.ustimov.messengerclient.responses;

import org.ustimov.messengerclient.events.IEvent;
import org.ustimov.messengerclient.models.Channel;
import org.ustimov.messengerclient.models.Message;
import org.ustimov.messengerclient.models.ResponseContact;
import org.ustimov.messengerclient.models.User;
import org.ustimov.messengerclient.services.Attach;

/**
 * Created by ustimov on 14/11/15.
 */
public class ResponseAggregator implements IResponse, IEvent {

    protected String status;
    protected String error;
    public String sid;
    public String uid;
    public String cid;
    public Channel[] channels;
    public User[] users;
    public Message[] last_msg;
    public String user_status;

    public String chid;
    public String from;
    public String nick;
    public String body;

    public String email;
    public String phone;
    public String picture;
    public ResponseContact[] list;
    public User user;
    public String time;
    public Attach attach;

    public ResponseAggregator() {}

    public ResponseAggregator(String status, String error) {
        this.status = status;
        this.error = error;
    }

    @Override
    public String getStatus() {
        return status;
    }

    @Override
    public String getError() {
        return error;
    }
}
