package org.ustimov.messengerclient.responses;

import org.ustimov.messengerclient.models.ResponseContact;

/**
 * Created by ustimov on 05/01/16.
 */
public class ContactListResponse extends ResponseAggregator {
    public ContactListResponse(String status, String error, ResponseContact[] contacts) {
        super(status, error);
        list = contacts;
    }

    public ResponseContact[] getContacts() {
        return list;
    }
}