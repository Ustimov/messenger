package org.ustimov.messengerclient.responses;

import org.ustimov.messengerclient.requests.AuthRequest;

/**
 * Created by ustimov on 08/11/15.
 */
public class AuthResponse extends ResponseAggregator {

    public AuthResponse(String status, String error, String userId, String sessionId) {
        super(status, error);
        cid = userId;
        sid = sessionId;
    }

    public String getUserId() {
        return cid;
    }

    public String getSessionId() {
        return sid;
    }
}
