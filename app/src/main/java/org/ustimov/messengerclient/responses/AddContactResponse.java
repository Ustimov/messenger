package org.ustimov.messengerclient.responses;

import org.ustimov.messengerclient.models.User;

/**
 * Created by ustimov on 05/01/16.
 */
public class AddContactResponse extends ResponseAggregator {
    public AddContactResponse(String status, String error, User user) {
        super(status, error);
        this.user = user;
    }

    public String getUserId() {
        return cid;
    }

    public String getSessionId() {
        return sid;
    }
}
