package org.ustimov.messengerclient.responses.depricated;

import org.ustimov.messengerclient.responses.ResponseAggregator;

/**
 * Created by ustimov on 08/11/15.
 */
public class LeaveChannelResponse extends ResponseAggregator {
    public LeaveChannelResponse(String status, String error) {
        super(status, error);
    }
}
