package org.ustimov.messengerclient.responses.depricated;

import org.ustimov.messengerclient.responses.ResponseAggregator;

/**
 * Created by ustimov on 22/11/15.
 */
public class CreateChannelResponse extends ResponseAggregator {
    public CreateChannelResponse(String status, String error, String channelId) {
        super(status, error);
        chid = channelId;
    }

    public String getChannelId() {
        return chid;
    }
}
