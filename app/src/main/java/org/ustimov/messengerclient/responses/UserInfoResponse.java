package org.ustimov.messengerclient.responses;

/**
 * Created by ustimov on 08/11/15.
 */
public class UserInfoResponse extends ResponseAggregator {

    public UserInfoResponse(
            String nickname, String userStatus, String email, String phone, String picture) {
        this.nick = nickname;
        this.user_status = userStatus;
        this.email = email;
        this.phone = phone;
        this.picture = picture;
    }

    public String getNickname() {
        return nick;
    }

    public String getUserStatus() {
        return user_status;
    }

    public String getEmail() { return email; }

    public String getPhone() { return phone; }

    public String getPicture() { return picture; }
}
