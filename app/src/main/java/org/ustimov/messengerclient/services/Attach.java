package org.ustimov.messengerclient.services;

/**
 * Created by ustimov on 05/01/16.
 */
public class Attach {

    public Attach(String mime, String data) {
        this.mime = mime;
        this.data = data;
    }

    protected String mime;
    protected String data;

    public String getType() {
        return mime;
    }

    public String getData() {
        return data;
    }
}
