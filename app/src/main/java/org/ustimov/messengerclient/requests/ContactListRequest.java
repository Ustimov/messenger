package org.ustimov.messengerclient.requests;

import org.ustimov.messengerclient.Actions;
import org.ustimov.messengerclient.AuthData;

/**
 * Created by ustimov on 05/01/16.
 */
public class ContactListRequest extends AuthData implements IRequest {

    private transient String mAction = Actions.CONTACTLIST;

    @Override
    public String getAction() {
        return mAction;
    }
}

