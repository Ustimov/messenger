package org.ustimov.messengerclient.requests;

import org.ustimov.messengerclient.Actions;
import org.ustimov.messengerclient.AuthData;

/**
 * Created by ustimov on 22/11/15.
 */
public class SetUserInfoRequest extends AuthData implements IRequest {

    private transient String mAction = Actions.SETUSERINFO;

    protected String user_status;
    protected String email;
    protected String phone;
    protected String picture;

    public SetUserInfoRequest(String userStatus, String email, String phone, String picture) {
        this.user_status = userStatus;
        this.email = email;
        this.phone = phone;
        this.picture = picture;
    }

    @Override
    public String getAction() {
        return mAction;
    }

    public String getUserStatus() {
        return user_status;
    }
}
