package org.ustimov.messengerclient.requests;

import org.ustimov.messengerclient.Actions;

/**
 * Created by ustimov on 08/11/15.
 */
public class AuthRequest implements IRequest {

    private transient String mAction = Actions.AUTH;

    public String login;
    public String pass;

    public AuthRequest(String login, String password) {
        this.login = login;
        this.pass = password;
    }

    @Override
    public String getAction() {
        return mAction;
    }
}
