package org.ustimov.messengerclient.requests.deprecated;

import org.ustimov.messengerclient.Actions;
import org.ustimov.messengerclient.AuthData;
import org.ustimov.messengerclient.requests.IRequest;

/**
 * Created by ustimov on 08/11/15.
 */
public class ChannelListRequest extends AuthData implements IRequest {

    private transient String mAction = Actions.CHANNELLIST;

    /*
    public ChannelListRequest(String userId, String sessionId) {
        cid = userId;
        sid = sessionId;
    }
    */

    @Override
    public String getAction() {
        return mAction;
    }
}
