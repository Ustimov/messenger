package org.ustimov.messengerclient.requests;

import org.ustimov.messengerclient.Actions;
import org.ustimov.messengerclient.AuthData;

/**
 * Created by ustimov on 05/01/16.
 */
public class AddContactRequest extends AuthData implements IRequest {

    private transient String mAction = Actions.ADDCONTACT;

    protected String uid;

    public AddContactRequest(String userId) {
        uid = userId;
    }

    @Override
    public String getAction() {
        return mAction;
    }

    public String getUserId() {
        return uid;
    }
}
