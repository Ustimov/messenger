package org.ustimov.messengerclient.requests;

import org.ustimov.messengerclient.Actions;
import org.ustimov.messengerclient.AuthData;

/**
 * Created by ustimov on 08/11/15.
 */
public class UserInfoRequest extends AuthData implements IRequest {

    private transient String mAction = Actions.USERINFO;

    public String user;

    public UserInfoRequest(String userId) {
        user = userId;
    }

    @Override
    public String getAction() {
        return mAction;
    }

}
