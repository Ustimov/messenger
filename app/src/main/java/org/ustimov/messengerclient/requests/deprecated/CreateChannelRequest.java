package org.ustimov.messengerclient.requests.deprecated;

import org.ustimov.messengerclient.Actions;
import org.ustimov.messengerclient.AuthData;
import org.ustimov.messengerclient.requests.IRequest;

/**
 * Created by ustimov on 22/11/15.
 */
public class CreateChannelRequest extends AuthData implements IRequest {

    private transient String mAction = Actions.CREATECHANNEL;

    protected String name;
    protected String descr;

    public CreateChannelRequest(String channelName, String channelDescription) {
        name = channelName;
        descr = channelDescription;
    }

    @Override
    public String getAction() {
        return mAction;
    }

    public String getChannelName() {
        return name;
    }

    public String getChannelDescription() {
        return descr;
    }

}
