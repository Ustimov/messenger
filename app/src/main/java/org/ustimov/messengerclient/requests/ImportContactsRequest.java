package org.ustimov.messengerclient.requests;

import org.ustimov.messengerclient.Actions;
import org.ustimov.messengerclient.AuthData;
import org.ustimov.messengerclient.models.RequestContact;

import java.util.ArrayList;

/**
 * Created by ustimov on 05/01/16.
 */
public class ImportContactsRequest extends AuthData implements IRequest {
    private transient String mAction = Actions.IMPORT;

    protected ArrayList<RequestContact> contacts;

    public ImportContactsRequest(ArrayList<RequestContact> contacts) {
        this.contacts = contacts;
    }

    @Override
    public String getAction() {
        return mAction;
    }

    public ArrayList<RequestContact> getContacts() {
        return contacts;
    }
}
