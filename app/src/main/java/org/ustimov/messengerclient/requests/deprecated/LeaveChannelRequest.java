package org.ustimov.messengerclient.requests.deprecated;

import org.ustimov.messengerclient.Actions;
import org.ustimov.messengerclient.AuthData;
import org.ustimov.messengerclient.requests.IRequest;

/**
 * Created by ustimov on 08/11/15.
 */
public class LeaveChannelRequest extends AuthData implements IRequest {

    private transient String mAction = Actions.LEAVE;

    public String channel;

    public LeaveChannelRequest(String channelId) {
        channel = channelId;
    }

    @Override
    public String getAction() {
        return mAction;
    }

}
