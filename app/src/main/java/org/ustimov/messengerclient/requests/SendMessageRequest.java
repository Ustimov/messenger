package org.ustimov.messengerclient.requests;

import org.ustimov.messengerclient.Actions;
import org.ustimov.messengerclient.AuthData;
import org.ustimov.messengerclient.services.Attach;

/**
 * Created by ustimov on 08/11/15.
 */
public class SendMessageRequest extends AuthData implements IRequest {

    private transient String mAction = Actions.MESSAGE;

    public String uid;
    public String body;
    public Attach attach;

    public SendMessageRequest(String userId, String message, Attach attach) {
        this.uid = userId;
        this.body = message;
        this.attach = attach;
    }

    @Override
    public String getAction() {
        return mAction;
    }
}
