package org.ustimov.messengerclient.requests;

import org.ustimov.messengerclient.Actions;
import org.ustimov.messengerclient.AuthData;

/**
 * Created by ustimov on 05/01/16.
 */
public class DeleteContactRequest extends AuthData implements IRequest {

    private transient String mAction = Actions.DELETECONTACT;

    protected String uid;

    public DeleteContactRequest(String userId) {
        uid = userId;
    }

    @Override
    public String getAction() {
        return mAction;
    }

    public String getUserId() {
        return uid;
    }
}