package org.ustimov.messengerclient;

import org.ustimov.messengerclient.events.*;
import org.ustimov.messengerclient.events.deprecated.ChatRoomEnterEvent;
import org.ustimov.messengerclient.events.deprecated.ChatRoomLeaveEvent;
import org.ustimov.messengerclient.responses.*;
import org.ustimov.messengerclient.responses.depricated.ChannelListResponse;
import org.ustimov.messengerclient.responses.depricated.CreateChannelResponse;
import org.ustimov.messengerclient.responses.depricated.EnterChannelResponse;
import org.ustimov.messengerclient.responses.depricated.LeaveChannelResponse;

import java.lang.reflect.Type;

/**
 * Created by ustimov on 08/11/15.
 */
public class Actions {
    public static final String REGISTER = "register";
    public static final String AUTH = "auth";
    public static final String CHANNELLIST = "channellist";
    public static final String USERINFO = "userinfo";
    public static final String ENTER = "enter";
    public static final String LEAVE = "leave";
    public static final String MESSAGE = "message";
    public static final String WELCOME = "welcome";
    public static final String CREATECHANNEL = "createchannel";
    public static final String SETUSERINFO = "setuserinfo";
    public static final String EV_ENTER = "ev_enter";
    public static final String EV_LEAVE = "ev_leave";
    public static final String EV_MESSAGE = "ev_message";

    public static final String CONTACTLIST = "contactlist";
    public static final String ADDCONTACT = "addcontact";
    public static final String DELETECONTACT = "delcontact";
    public static final String IMPORT = "import";

    public static boolean isEvent(String action) {
        return action.equals(WELCOME) || action.substring(0, 2).equals("ev");
    }

    public static IResponse getResponse(String action, ResponseAggregator response) {
        switch (action) {
            case REGISTER:
                return new RegisterResponse(response.getStatus(), response.getError());
            case AUTH:
                return new AuthResponse(response.getStatus(), response.getError(), response.cid,
                        response.sid);
            case CHANNELLIST:
                return new ChannelListResponse(response.getStatus(),response.getError(),
                        response.channels);
            case ENTER:
                return new EnterChannelResponse(response.getStatus(), response.getError(),
                        response.users, response.last_msg);
            case LEAVE:
                return new LeaveChannelResponse(response.getStatus(), response.getError());
            case USERINFO:
                return new UserInfoResponse(response.nick, response.user_status, response.email,
                        response.phone, response.picture);
            case SETUSERINFO:
                return new SetUserInfoResponse(response.getStatus(), response.getError());
            case CREATECHANNEL:
                return new CreateChannelResponse(response.getStatus(), response.getError(),
                        response.chid);
            case CONTACTLIST:
                return new ContactListResponse(response.getStatus(), response.getError(),
                        response.list);
            case DELETECONTACT:
                return new DeleteContactResponse(response.getStatus(), response.getError(),
                        response.uid);
            case ADDCONTACT:
                return new AddContactResponse(response.getStatus(), response.getError(),
                        response.user);
            case IMPORT:
                return new ImportContactsResponse(response.getStatus(), response.getError(),
                        response.list);
        }
        return response;
    }

    public static IEvent getEvent(String action, ResponseAggregator response) {
        switch (action) {
            case EV_MESSAGE:
                return new SentMessageEvent(response.from, response.nick, response.body,
                        response.time, response.attach);
        }
        return response;
    }
}
