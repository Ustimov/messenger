package org.ustimov.messengerclient;

public abstract class Frame {
    public String action;

    public String getAction() {
        return action;
    }
}
